using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementUnlockedPopupContainer : MonoBehaviour
{
    public static AchievementUnlockedPopupContainer Instance
    {
        get
        {
            return _Instance;
        }
    }

    private static AchievementUnlockedPopupContainer _Instance;

    [SerializeField]
    private GameObject _achievementUnlockedPopup;

    private AchievementMenuElement[] _achievements;

    void Awake()
    {
        _Instance = this;
        _achievements = FindObjectsOfType<AchievementMenuElement>(true);
    }

    public void ShowAchievementUnlockedPopup(string achievementName)
    {
        bool isDebugAchievement = achievementName.Contains("Debug");

        if (isDebugAchievement == false && PlayerPrefs.GetInt("AchievementUnlocked:" + achievementName, 0) == 1)
        {
            Debug.LogWarning("The user has already been shown the \"achievement unlocked\" popup for " + achievementName);
            return;
        }

        GameObject popup = Instantiate(_achievementUnlockedPopup, transform);

        string popupAchievementName = FindAchievementTranslation(achievementName);

        popup.GetComponent<AchievementUnlockedPopup>().StartAchievementUnlockedPopupAnimation(popupAchievementName, transform.childCount);

        if (isDebugAchievement == false)
        {
            PlayerPrefs.SetInt("AchievementUnlocked:" + achievementName, 1);
        }
    }

    private string FindAchievementTranslation(string achievementName)
    {
        foreach (AchievementMenuElement achievement in _achievements)
        {
            if (achievement.GetAchievementIdentifier() == achievementName)
            {
                return achievement.GetAchievementTranslations()[LanguageManager.GetPlayerPrefsLanguageIndex()];
            }
        }

        return achievementName;
    }
}
