using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AchievementUnlockedPopup : MonoBehaviour
{
    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private LanguageDependentElement _achievementUnlockedLabel;

    [SerializeField]
    private TextMeshProUGUI _achievementName;

    public void StartAchievementUnlockedPopupAnimation(string achievementName, int childCount)
    {
        _achievementUnlockedLabel.UpdateElement(LanguageManager.Instance.GameLanguage);
        _achievementName.text = achievementName;
        StartCoroutine(ShowAchievementUnlockedPopup(childCount));
    }

    private IEnumerator ShowAchievementUnlockedPopup(int childCount)
    {
        yield return new WaitForSeconds(4f * (childCount - 1));

        _animator.Play("ShowAchievementUnlockedPopup");

        yield return new WaitUntil(() => _animator.GetCurrentAnimatorStateInfo(0).IsName("ShowAchievementUnlockedPopup"));
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);

        Destroy(gameObject);
    }
}
