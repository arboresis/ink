using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Category : MonoBehaviour
{
    [HideInInspector]
    public LevelManager levelManager;

    [SerializeField]
    private GameObject GeneratedLevelButton;

    [SerializeField]
    private Transform LevelButtonParent;

    [SerializeField]
    private TextMeshProUGUI _categoryNameDisplay;

    [SerializeField]
    private List<Button> _categoryButtons = new List<Button>();

    public string CategoryName;

    [HideInInspector]
    public string[] CategoryNameTranslations;

    private void OnEnable()
    {
        if (CategoryNameTranslations.Length > 1)
        {
            _categoryNameDisplay.text = CategoryNameTranslations[LanguageManager.GetPlayerPrefsLanguageIndex()];
        }
    }

    public void SetCategoryName(string name)
    {
        _categoryNameDisplay.text = name;
        CategoryName = name;
    }

    public LevelButton AddLevelButton(string levelName, int levelIndex)
    {
        LevelButton levelButton = Instantiate(original: GeneratedLevelButton, parent: LevelButtonParent).GetComponent<LevelButton>();
        levelButton.SetParameters(levelName, levelIndex);
        levelButton.SetButtonEvent(() =>
        {
            levelManager.SelectLevel(levelIndex - 1);
            levelManager.HideLevelSelectionMenu();
        });

        _categoryButtons.Add(levelButton.GetComponent<Button>());

        return levelButton;
    }

    public void SetCategoryNameTranslations(string[] translations)
    {
        CategoryNameTranslations = translations;

        if (CategoryNameTranslations.Length > 1)
        {
            _categoryNameDisplay.text = CategoryNameTranslations[LanguageManager.GetPlayerPrefsLanguageIndex()];
        }
    }
}
