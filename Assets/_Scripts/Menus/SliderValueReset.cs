using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderValueReset : MonoBehaviour
{
    [SerializeField]
    private Slider _slider;

    [SerializeField]
    private string _sliderSettingName;

    [SerializeField]
    private float _defaultSettingValue = 0f;

    // Start is called before the first frame update
    void OnEnable()
    {
        _slider.value = PlayerPrefs.GetFloat(_sliderSettingName, _defaultSettingValue);
    }
}
