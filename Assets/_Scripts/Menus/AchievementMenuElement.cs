using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AchievementMenuElement : MonoBehaviour
{
    [SerializeField]
    private string _achievementIdentifier;

    [Space]
    [SerializeField] private GameObject _achievementLockedVersion;
    [SerializeField] private GameObject _achievementUnlockedVersion;

    [Space]
    [SerializeField] private string _achievementTitle;
    [SerializeField] private string _achievementDescription;

    [Space]
    [SerializeField] private string[] _achievementTitles;
    [SerializeField] private string[] _achievementDescriptions;

    [Space]
    [SerializeField] private TextMeshProUGUI _lockedAchievementTitle;
    [SerializeField] private TextMeshProUGUI _lockedAchievementDescription;
    [SerializeField] private TextMeshProUGUI _unlockedAchievementTitle;
    [SerializeField] private TextMeshProUGUI _unlockedachievementDescription;

    private void Awake()
    {
        _lockedAchievementTitle.text = _achievementTitles[0];
        _unlockedAchievementTitle.text = _achievementTitles[0];

        _lockedAchievementDescription.text = _achievementDescriptions[0];
        _unlockedachievementDescription.text = _achievementDescriptions[0];
    }

    private void OnEnable()
    {
        UpdateAchievementNameAndDescriptionLanguage();

        if(PlayerPrefs.GetInt("AchievementUnlocked:" + _achievementIdentifier, 0) == 1)
        {
            _achievementLockedVersion.SetActive(false);
            _achievementUnlockedVersion.SetActive(true);
        }
        else
        {
            _achievementLockedVersion.SetActive(true);
            _achievementUnlockedVersion.SetActive(false);
        }
    }

    private void UpdateAchievementNameAndDescriptionLanguage()
    {
        int index = (int)LanguageManager.Instance.GameLanguage;

        _lockedAchievementTitle.text = _achievementTitles[index];
        _unlockedAchievementTitle.text = _achievementTitles[index];

        _lockedAchievementDescription.text = _achievementDescriptions[index];
        _unlockedachievementDescription.text = _achievementDescriptions[index];
    }

    public string GetAchievementIdentifier()
    {
        return _achievementIdentifier;
    }

    public string[] GetAchievementTranslations()
    {
        return _achievementTitles;
    }
}
