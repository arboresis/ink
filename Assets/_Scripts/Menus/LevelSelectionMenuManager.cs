using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectionMenuManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _category;

    [SerializeField]
    private LevelManager _levelManager;

    private List<Level> _levelList;

    private string _lastCategoryName;
    private Category _currentCategory;

    [SerializeField]
    private List<LevelButton> _levelButtons;

    [SerializeField]
    private float _minimumPercentageToShowCategoryName;

    private List<Category> _categories;

    // Start is called before the first frame update
    void Awake()
    {
        _levelList = new List<Level>();
        _levelList = _levelManager.GetLevelList();

        _levelButtons = new List<LevelButton>();
        CreateLevelSelectionMenu();
    }

    private void OnEnable()
    {
        CheckEveryButtonInteractable();
    }

    private void CheckEveryButtonInteractable()
    {
        float percentage = 0f;

        int furthestPlayedLevel = PlayerPrefs.GetInt("FurthestLevel", 0);
        int lastPlayedLevel = PlayerPrefs.GetInt("LastPlayedLevel", 0);

        for (int i = 0; i < _levelButtons.Count; i++)
        {
            percentage = PlayerPrefs.GetFloat(_levelButtons[i].LevelName, 0f);

            _levelButtons[i].UpdatePercentage(percentage);

            _levelButtons[i].SetButtonInteractable(i <= furthestPlayedLevel);

            _levelButtons[i].SetActiveLevelButton(i == lastPlayedLevel);
        }
    }

    public RectTransform GetActiveButtonTransform()
    {
        int lastPlayedLevel = PlayerPrefs.GetInt("LastPlayedLevel", 0);

        for (int i = 0; i < _levelButtons.Count; i++)
        {
            if (i == lastPlayedLevel)
            {
                return _levelButtons[i].GetComponent<RectTransform>();
            }
        }

        return _levelButtons[0].GetComponent<RectTransform>();
    }

    private void CreateLevelSelectionMenu()
    {
        _categories = new List<Category>();

        for (int i = 0; i < _levelList.Count; i++)
        {
            if (_levelList[i].CategoryName != _lastCategoryName)
            {
                _lastCategoryName = _levelList[i].CategoryName;

                _currentCategory = Instantiate(original: _category, parent: transform).GetComponent<Category>();
                _currentCategory.levelManager = _levelManager;
                _currentCategory.SetCategoryName(_lastCategoryName);
                _categories.Add(_currentCategory);
            }

            _levelButtons.Add(_currentCategory.AddLevelButton(_levelList[i].LevelName, i + 1));
        }

        SetupCategoryTranslations();
    }

    //WARNING: THIS IS NOT GOOD CODE. IT'S PRETTY SHITTY, IN FACT :V
    private void SetupCategoryTranslations()
    {
        foreach (Category category in _categories)
        {
            switch (category.CategoryName)
            {
                case "Basic Shapes":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Basic Shapes",
                            "Formes Basiques",
                            "",
                            "",
                            "",
                            "Figuras B�sicas",
                            "",
                            "",
                            "Formas B�sicas"
                        });
                    break;
                case "Origami Animals":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Origami Animals",
                            "Animaux Origami",
                            "",
                            "",
                            "",
                            "Animais Origami",
                            "",
                            "",
                            "Animales de Origami"
                        });
                    break;
                case "Work Tools":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Work Tools",
                            "Outils de Travail",
                            "",
                            "",
                            "",
                            "Ferramentas de Trabalho",
                            "",
                            "",
                            "Herramientas de Trabajo"
                        });
                    break;
                case "Furniture":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Furniture",
                            "Ameublement",
                            "",
                            "",
                            "",
                            "Mob�lia",
                            "",
                            "",
                            "Mobiliario"
                        });
                    break;
                case "Household Items":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Household Items",
                            "Articles de M�nage",
                            "",
                            "",
                            "",
                            "Itens Dom�sticos",
                            "",
                            "",
                            "Art�culos del Hogar"
                        });
                    break;
                case "Transport Methods":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Transport Methods",
                            "M�thodes de Transport",
                            "",
                            "",
                            "",
                            "M�todos de Transporte",
                            "",
                            "",
                            "Medios de Transporte"
                        });
                    break;
                case "Clothing Items":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Clothing Items",
                            "V�tements",
                            "",
                            "",
                            "",
                            "Itens de Vestu�rio",
                            "",
                            "",
                            "Art�culos de Ropa"
                        });
                    break;
                case "Animals":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Animals",
                            "Animaux",
                            "",
                            "",
                            "",
                            "Animais",
                            "",
                            "",
                            "Animales"
                        });
                    break;
                case "Instruments":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Instruments",
                            "Instruments",
                            "",
                            "",
                            "",
                            "Instrumentos",
                            "",
                            "",
                            "Instrumentos"
                        });
                    break;
                case "Sports":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Sports",
                            "Activit�s Sportives",
                            "",
                            "",
                            "",
                            "Desportos",
                            "",
                            "",
                            "Deportes"
                        });
                    break;
                case "Flowers":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Flowers",
                            "Fleurs",
                            "",
                            "",
                            "",
                            "Flores",
                            "",
                            "",
                            "Flores"
                        });
                    break;
                case "Birds":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Birds",
                            "Oiseaux",
                            "",
                            "",
                            "",
                            "P�ssaros",
                            "",
                            "",
                            "P�jaros"
                        });
                    break;
                case "Continents":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Continents",
                            "Continents",
                            "",
                            "",
                            "",
                            "Continentes",
                            "",
                            "",
                            "Continentes"
                        });
                    break;
                case "Locations":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Locations",
                            "Lieux",
                            "",
                            "",
                            "",
                            "Locais",
                            "",
                            "",
                            "Ubicaciones"
                        });
                    break;
                case "Albums":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Albums",
                            "Albums",
                            "",
                            "",
                            "",
                            "�lbuns",
                            "",
                            "",
                            "�lbumes"
                        });
                    break;
                case "Paintings":
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "Paintings",
                            "Peintures",
                            "",
                            "",
                            "",
                            "Pinturas",
                            "",
                            "",
                            "Pinturas"
                        });
                    break;
                default:
                    category.SetCategoryNameTranslations(
                        new string[]
                        {
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            ""
                        });
                    break;
            }
        }
    }
}