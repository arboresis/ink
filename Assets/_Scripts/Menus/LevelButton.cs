using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _levelCounter;

    [SerializeField]
    private Image _levelPercentageCounter;

    [SerializeField]
    private GameObject _activeLevelIndicator;

    private Button _button;

    public float LevelPercentage;

    public string LevelName;

    private void Awake()
    {
        _button = GetComponent<Button>();
    }

    public void SetParameters(string levelName, int levelIndex)
    {
        LevelName = levelName;
        _levelCounter.text = levelIndex.ToString();
        UpdatePercentage(PlayerPrefs.GetFloat(LevelName, 0f));
    }

    public void UpdatePercentage(float levelPercentage)
    {
        LevelPercentage = levelPercentage;
        _levelPercentageCounter.fillAmount = LevelPercentage;
    }

    public void SetButtonInteractable(bool interactable)
    {
        _button.interactable = interactable;
    }

    public void SetActiveLevelButton(bool activeLevelButton)
    {
        _activeLevelIndicator.SetActive(activeLevelButton);
    }

    public void SetButtonEvent(UnityAction action)
    {
        _button.onClick.RemoveAllListeners();
        _button.onClick.AddListener(action);
    }
}
