﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Linq;
using UnityEngine.Events;
using PaintIn3D;
using UnityEditor;
using TARGET = P3dTapThrow;

/// <summary>This component will spawn and throw Rigidbody prefabs from the camera when you tap the mouse or a finger.</summary>
[HelpURL(P3dHelper.HelpUrlPrefix + "P3dTapThrow")]
[AddComponentMenu(P3dHelper.ComponentMenuPrefix + "Tap Throw")]
public class P3dTapThrow : MonoBehaviour
{
    private Transform ThrowOrigin;

    /// <summary>The key that must be held for this component to activate on desktop platforms.
    /// None = Any mouse button.</summary>
    public KeyCode Key { set { key = value; } get { return key; } }
    [SerializeField] private KeyCode key = KeyCode.Mouse0;

    /// <summary>Fingers that began touching the screen on top of these UI layers will be ignored.</summary>
    public LayerMask GuiLayers { set { guiLayers = value; } get { return guiLayers; } }
    [SerializeField] private LayerMask guiLayers = 1 << 5;

    /// <summary>The prefab that will be thrown.</summary>
    public GameObject Prefab { set { prefab = value; } get { return prefab; } }
    [SerializeField] private GameObject prefab;

    /// <summary>The speed that the object will be thrown at.</summary>
    public float Speed { set { speed = value; } get { return speed; } }
    [SerializeField] private float speed = 10.0f;

    /// <summary>Should painting triggered from this component be eligible for being undone?</summary>
    public bool StoreStates { set { storeStates = value; } get { return storeStates; } }
    [SerializeField] protected bool storeStates;

    [System.NonSerialized]
    private List<P3dInputManager.Finger> fingers = new List<P3dInputManager.Finger>();

    private Camera _camera;

    private LineRenderer _lineRenderer;

    private float _deltaModifier;

    public Vector3 GravityForce;

    [HideInInspector]
    public float
        MinimumSpeed,
        MaximumSpeed;

    public float TrajectoryPredictionInterval = 0.1f;

    public GameObject TrajectoryPredictorsParent;

    public List<Transform> TrajectoryPredictors;

    public UnityEvent BallSizeSelectionHideEvent;

    private Vector3 _throwTarget;
    private Vector3 _throwForce;

    public float InkBallMaximumLifetime = 5f;

    public LevelManager levelManager;

    private bool _dragging = false;

    [SerializeField]
    private BallSizeSelectionMenu _ballSizeSelectionMenu;

    private Vector2
        fingerStartPosition,
        fingerEndPosition,
        normalizedStartPosition,
        normalizedEndPosition;

    [SerializeField]
    private float _minimumPercentageOfScreenToDetectDrag = 5f;

    private float percentageOfScreen;

    [SerializeField]
    private MultiTargetGraphicButton _cancelThrowIcon;

    public AudioClip
        PredictionTickSound,
        BallReleaseSound,
        BallHitSound;

    [Space]
    [SerializeField] private AudioClip[] _sillyReleaseSounds;
    [SerializeField] private AudioClip[] _sillyHitSounds;

    [Space]
    [SerializeField] private Color[] _rainbowColors;

    private int
        _lastUsedReleaseSound = 0,
        _lastUsedHitSound = 0;

    private GameObject
        _predictionTickSoundObject,
        _ballReleaseSoundObject;

    private void Awake()
    {
        //Square the percentage needed
        _minimumPercentageOfScreenToDetectDrag = Mathf.Pow(_minimumPercentageOfScreenToDetectDrag, 2f);

        ThrowOrigin = GameObject.FindGameObjectWithTag("ThrowOrigin")?.transform;
        _lineRenderer = ThrowOrigin.GetComponent<LineRenderer>();
        _lineRenderer.enabled = false;
    }

    protected virtual void OnEnable()
    {
        P3dInputManager.EnsureThisComponentExists();

        P3dInputManager.OnFingerDown += HandleFingerDown;
        P3dInputManager.OnFingerUp += HandleFingerUp;
        P3dInputManager.OnFingerUpdate += OnFingerUpdate;

        _camera = P3dHelper.GetCamera();
    }

    protected virtual void OnDisable()
    {
        P3dInputManager.OnFingerDown -= HandleFingerDown;
        P3dInputManager.OnFingerUp -= HandleFingerUp;
        P3dInputManager.OnFingerUpdate -= OnFingerUpdate;
    }

    private void HandleFingerDown(P3dInputManager.Finger finger)
    {
        if (GameManager.ShouldAllowGameControl == false)
        {
            return;
        }

        if (finger.Index == P3dInputManager.HOVER_FINGER_INDEX) return;

        if (P3dInputManager.PointOverGui(finger.ScreenPosition, guiLayers) == true) return;

        if (key != KeyCode.None && P3dInputManager.IsDown(key) == false) return;

        fingers.Add(finger);

        ThrowOrigin.position = (Vector2)ScreenToWorldPoint(finger.ScreenPosition);

        TrajectoryPredictors = new List<Transform>(TrajectoryPredictorsParent.transform.childCount - 1);
        TrajectoryPredictors = TrajectoryPredictorsParent.GetComponentsInChildren<Transform>().ToList();
        TrajectoryPredictors.Remove(TrajectoryPredictors[0]);

        fingerStartPosition = new Vector2
                                        (
                                            finger.StartScreenPosition.x / Screen.width * 100f,
                                            finger.StartScreenPosition.y / Screen.height * 100f
                                        );

        normalizedStartPosition = new Vector2
                                        (
                                            finger.StartScreenPosition.x / Screen.height * 100f,
                                            finger.StartScreenPosition.y / Screen.height * 100f
                                        );

        RemoveFingerIfOutsideScreen(finger);
    }

    private void RemoveFingerIfOutsideScreen(P3dInputManager.Finger finger)
    {
        if (fingerStartPosition.x < 0f || fingerStartPosition.x > 100f || fingerStartPosition.y < 0f || fingerStartPosition.y > 100f)
        {
            fingers.Remove(finger);
        }
    }

    private void OnFingerUpdate(P3dInputManager.Finger finger)
    {
        if (GameManager.ShouldAllowGameControl == false || fingers.Contains(finger) == false)
        {
            return;
        }

        fingerEndPosition = new Vector2
                                        (
                                            finger.ScreenPosition.x / Screen.width * 100f,
                                            finger.ScreenPosition.y / Screen.height * 100f
                                        );

        normalizedEndPosition = new Vector2
                                        (
                                            finger.ScreenPosition.x / Screen.height * 100f,
                                            finger.ScreenPosition.y / Screen.height * 100f
                                        );

        float previousPercentage = percentageOfScreen;

        percentageOfScreen = (normalizedEndPosition - normalizedStartPosition).sqrMagnitude;
        //LOG: Debug.LogWarning(percentageOfScreen + "\n" + (fingerEndPosition - fingerStartPosition));
        _dragging = percentageOfScreen > _minimumPercentageOfScreenToDetectDrag;

        if(_dragging == false)
        {
            _lineRenderer.enabled = false;
            TrajectoryPredictorsParent.SetActive(false);
            _cancelThrowIcon.interactable = true;
            return;
        }
        else
        {
            TrajectoryPredictorsParent.SetActive(true);
            _cancelThrowIcon.interactable = false;
        }

        if (PlayerPrefs.GetInt("ThrowDirection", 1) == -1)
        {
            _lineRenderer.SetPosition(0, ThrowOrigin.position);
            _lineRenderer.SetPosition(1, (Vector2)ScreenToWorldPoint(finger.ScreenPosition));
        }
        else
        {
            _lineRenderer.SetPosition(1, ThrowOrigin.position);
            _lineRenderer.SetPosition(0, (Vector2)ScreenToWorldPoint(finger.ScreenPosition));
        }

        _lineRenderer.enabled = true;
        TrajectoryPredictorsParent.SetActive(true);

        _deltaModifier = GetDeltaModifier(percentageOfScreen);

        _throwTarget = (Vector2)ScreenToWorldPoint(finger.ScreenPosition);
        _throwForce = _deltaModifier * Speed * ((Vector3)_throwTarget - ThrowOrigin.position).normalized * PlayerPrefs.GetInt("ThrowDirection", 1);

        for (int i = 0; i < TrajectoryPredictors.Count; i++)
        {
            TrajectoryPredictors[i].position = CalculatePosition(TrajectoryPredictionInterval * (i + 1));
        }

        if (Mathf.Abs(previousPercentage - percentageOfScreen) >= 1.5f && _predictionTickSoundObject == null)
        {
            _predictionTickSoundObject = AudioManager.Instance.PlaySound(PredictionTickSound, false, AudioEventType.SoundEffect, 0.5f).gameObject;
        }

        BallSizeSelectionHideEvent.Invoke();
    }

    private Vector3 CalculatePosition(float elapsedTime)
    {
        return ThrowOrigin.position +
        _throwForce * Time.fixedDeltaTime * elapsedTime +
        0.5f * GravityForce * elapsedTime * elapsedTime * 1.025f;
    }

    private void HandleFingerUp(P3dInputManager.Finger finger)
    {
        if (GameManager.ShouldAllowGameControl == false)
        {
            return;
        }

        if (_dragging == false)
        {
            _lineRenderer.enabled = false;
            TrajectoryPredictorsParent.SetActive(false);
            _cancelThrowIcon.interactable = false;
            fingers.Remove(finger);
            return;
        }

        _dragging = false;

        if (levelManager.CanThrowInkBall() == false)
        {
            _lineRenderer.enabled = false;
            TrajectoryPredictorsParent.SetActive(false);
            fingers.Remove(finger);

            _ballSizeSelectionMenu.PlayNoBallsAnimation();

            return;
        }

        if (fingers.Remove(finger) == true)
        {
            DoThrow(finger.ScreenPosition);
        }

        if (CheatsManager.HasSillySoundEffectsEnabled() == false)
        {
            _ballReleaseSoundObject = AudioManager.Instance.PlaySound(BallReleaseSound, false, AudioEventType.SoundEffect).gameObject;
        }
        else
        {
            int randomReleaseSoundIndex = 0;

            do
            {
                randomReleaseSoundIndex = UnityEngine.Random.Range(0, _sillyReleaseSounds.Length - 1);
            }
            while (randomReleaseSoundIndex == _lastUsedReleaseSound);

            _lastUsedReleaseSound = randomReleaseSoundIndex;

            _ballReleaseSoundObject = AudioManager.Instance.PlaySound(_sillyReleaseSounds[randomReleaseSoundIndex], false, AudioEventType.SoundEffect).gameObject;
        }

        _lineRenderer.enabled = false;
        TrajectoryPredictorsParent.SetActive(false);

        ClearCachedFingers();
    }

    private void DoThrow(Vector2 screenPosition)
    {
        if (prefab != null)
        {
            if (_camera != null)
            {
                if (storeStates == true)
                {
                    P3dStateManager.StoreAllStates();
                }

                levelManager.DeductInkBall();

                // Loop through all prefabs and spawn them
                var clone = Instantiate(prefab, ThrowOrigin.position, Quaternion.identity);
                var p3dDestroyer = clone.GetComponent<P3dDestroyer>();

                levelManager.ActiveBalls.Add(clone);

                if (CheatsManager.HasInktinArboresitinoEnabled() && levelManager.IsInvertedLevel() == false)
                {
                    int randomColorIndex = UnityEngine.Random.Range(0, _rainbowColors.Length - 1);

                    clone.GetComponent<P3dPaintDecal>().SetBallColour(_rainbowColors[randomColorIndex]);
                }
                else
                {
                    clone.GetComponent<P3dPaintDecal>().SetBallColour(Color.black);
                }

                clone.SetActive(true);

                if (CheatsManager.HasSillySoundEffectsEnabled() == false)
                {
                    p3dDestroyer.InkBallHitSound.AddListener(() =>
                    {
                        AudioManager.Instance.PlaySound(BallHitSound, false, AudioEventType.SoundEffect);
                    });
                }
                else
                {
                    int randomHitSoundIndex = 0;

                    do
                    {
                        randomHitSoundIndex = UnityEngine.Random.Range(0, _sillyHitSounds.Length - 1);
                    }
                    while (randomHitSoundIndex == _lastUsedHitSound);

                    _lastUsedHitSound = randomHitSoundIndex;

                    p3dDestroyer.InkBallHitSound.AddListener(() =>
                    {
                        AudioManager.Instance.PlaySound(_sillyHitSounds[randomHitSoundIndex], false, AudioEventType.SoundEffect);
                    });
                }

                p3dDestroyer.OnDestroyEvent.AddListener(() =>
                {
                    levelManager.ActiveBalls.Remove(clone);
                });

                // Throw with velocity?
                var cloneRigidbody = clone.GetComponent<Rigidbody>();

                if (cloneRigidbody != null)
                {
                    cloneRigidbody.AddForce(_throwForce);
                    clone.GetComponent<ConstantForce>().force = GravityForce;
                    StartCoroutine(DestroyInkBallAfterTime(clone));
                }
            }
        }
    }

    private float GetDeltaModifier(float fingerDelta)
    {
        /*
         * The finger delta modifier is given by a linear function (y = m * x + b) given the two following points:
         * (MinimumSpeed, 2f)
         * (MaximumSpeed, 30f)
         */

        float m = (2f - 30f) / (MinimumSpeed - MaximumSpeed);
        float b = -(m * MinimumSpeed - 2f);

        return m * Mathf.Sqrt(fingerDelta) + b;
    }

    private Vector3 ScreenToWorldPoint(Vector2 screenPosition)
    {
        return _camera.ScreenToWorldPoint(screenPosition);
    }

    public void ClearCachedFingers(bool clearIndicators = false)
    {
        fingers.Clear();

        if (clearIndicators)
        {
            _lineRenderer.enabled = false;
            TrajectoryPredictorsParent.SetActive(false);
            _cancelThrowIcon.interactable = false;
        }
    }

    private IEnumerator DestroyInkBallAfterTime(GameObject inkBall)
    {
        yield return new WaitForSeconds(InkBallMaximumLifetime);

        if (inkBall != null)
        {
            inkBall.GetComponent<P3dDestroyer>().DestroyNow();
        }
    }
}

#if UNITY_EDITOR


[CanEditMultipleObjects]
[CustomEditor(typeof(TARGET))]
public class P3dTapThrow_Editor : P3dEditor
{
    protected override void OnInspector()
    {
        TARGET tgt; TARGET[] tgts; GetTargets(out tgt, out tgts);

        Draw("_minimumPercentageOfScreenToDetectDrag");

        Draw("key", "The key that must be held for this component to activate on desktop platforms.\n\nNone = Any mouse button.");
        Draw("guiLayers", "Fingers that began touching the screen on top of these UI layers will be ignored.");

        Separator();

        BeginError(Any(tgts, t => t.Prefab == null));
        Draw("prefab", "The prefab that will be thrown.");
        EndError();
        Draw("speed", "Rotate the decal to the hit normal?");
        Draw("storeStates", "Should painting triggered from this component be eligible for being undone?");

        Separator();

        Draw("GravityForce");

        Separator();

        Draw("MinimumSpeed");
        Draw("MaximumSpeed");

        Separator();

        Draw("InkBallMaximumLifetime");

        Separator();

        Draw("TrajectoryPredictionInterval");
        Draw("TrajectoryPredictorsParent");
        Draw("TrajectoryPredictors");

        Separator();

        Draw("BallSizeSelectionHideEvent");

        Separator();

        Draw("levelManager");

        Separator();

        Draw("_ballSizeSelectionMenu");

        Separator();

        Draw("_cancelThrowIcon");

        Separator();

        Draw("PredictionTickSound");

        Separator();

        Draw("_sillyReleaseSounds");
        Draw("_sillyHitSounds");

        Separator();

        Draw("_rainbowColors");
    }
}
#endif