using PaintIn3D;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public enum ForceType
{
    Regular,
    None,
    Inverted
}

public class Level : MonoBehaviour
{
    private const float LEVEL_COMPLETED_REVEAL_DURATION = 1f;
    private const float LEVEL_COMPLETED_WAIT = 2f;
    private const float LEVEL_COMPLETED_POST_ANIMATION_WAIT = 2f;
    private const string LEVEL_FINISHED_ANIMATOR_STATE_NAME = "LevelFinished";
    private const char LEVEL_NAME_SEPARATOR = ';';

    public bool Inverted = false;

    public ForceType LevelForceType = ForceType.Regular;

    [Space]
    [SerializeField]
    private int _amountOfSmallInkBalls = -1;
    [SerializeField]
    private int _amountOfMediumInkBalls = -1;
    [SerializeField]
    private int _amountOfBigInkBalls = -1;
    [SerializeField]
    private int _amountOfNukeInkBalls = -1;

    [Space]
    [SerializeField]
    private float _percentageNeeded = 100f;

    private RectTransform _percentageNeededIndicator;
    private RectTransform _maskedPercentageNeededIndicator;

    private P3dPaintableTexture[] _paintableTextures = new P3dPaintableTexture[0];

    private LevelManager _levelManager;

    [SerializeField]
    private bool _shouldUseEvent = false;

    [SerializeField]
    private UnityEvent _onLevelFadedOutEvent;

    private Color _originalColor;

    private Image _globalControlBlocker;

    //[HideInInspector]
    public string
        LevelName,
        CategoryName;

    private float _timeSinceLevelStart;

    // Start is called before the first frame update
    void Awake()
    {
        _paintableTextures = GetComponentsInChildren<P3dPaintableTexture>();
        _percentageNeededIndicator = GameObject.FindGameObjectWithTag("PercentageNeededIndicator").GetComponent<RectTransform>();
        _maskedPercentageNeededIndicator = GameObject.FindGameObjectWithTag("MaskedPercentageNeededIndicator").GetComponent<RectTransform>();
        _levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        _globalControlBlocker = GameObject.FindGameObjectWithTag("GlobalControlBlocker").GetComponent<Image>();
        _originalColor = _paintableTextures[0].Material.GetColor("_Color");

        if ((Application.isEditor || Debug.isDebugBuild) && _levelManager.ShouldUseDebugPercentageNeeded())
        {
            _percentageNeeded = 1f;
        }
    }

    private void OnEnable()
    {
        _levelManager.SetCurrentLevel(transform.GetSiblingIndex());
        _levelManager.SetTargetPercentage(_percentageNeeded);

        if (CheatsManager.HasInfiniteBallsEnabled() == false)
        {
            _levelManager.ResetInkBallsAmounts(_amountOfSmallInkBalls, _amountOfMediumInkBalls, _amountOfBigInkBalls, _amountOfNukeInkBalls);
        }
        else
        {
            _levelManager.ResetInkBallsAmounts(-1, -1, -1, -1);
        }
        
        _levelManager.ResetNextLevelButton();

        ResetLevel();
    }

    private void Update()
    {
        if (GameManager.ShouldAllowGameControl)
        {
            _timeSinceLevelStart += Time.deltaTime;
        }
    }

    public void GetLevelName()
    {
        string[] gameObjectName = gameObject.name.Split(LEVEL_NAME_SEPARATOR);

        CategoryName = gameObjectName[0];
        LevelName = gameObjectName[1];
    }

    private void ResetLevel()
    {
        foreach (P3dPaintableTexture paintableTexture in _paintableTextures)
        {
            if (paintableTexture != null)
            {
                if (_levelManager.IsInvertedLevel() == false)
                {
                    paintableTexture.Clear();
                }
                else
                {
                    paintableTexture.Color = Color.black;
                    paintableTexture.Clear();
                }

                paintableTexture.Material.SetColor("_Color", _originalColor);
            }
        }

        Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 0, _percentageNeeded / 100f * 360f * -1f));
        _percentageNeededIndicator.localRotation = targetRotation;
        _maskedPercentageNeededIndicator.localRotation = targetRotation;
        _timeSinceLevelStart = 0f;

        ToggleGlobalBlocker(false);
    }

    public void ResetCurrentLevel()
    {
        gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    public void LevelCompleted()
    {
        StartCoroutine(ShowEndResult());
    }

    private IEnumerator ShowEndResult()
    {
        ToggleGlobalBlocker(true);

        SavePercentageOfLevel();

        if (_levelManager.ShouldRevealEndResult())
        {
            float revealProgress = 0f;

            while (revealProgress < LEVEL_COMPLETED_REVEAL_DURATION)
            {
                revealProgress += Time.deltaTime;

                foreach (P3dPaintableTexture paintableTexture in _paintableTextures)
                {
                    if (paintableTexture != null)
                    {
                        paintableTexture.Material.SetColor("_Color", Color.Lerp(_originalColor, Color.black, revealProgress / LEVEL_COMPLETED_REVEAL_DURATION));
                    }
                }

                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSeconds(LEVEL_COMPLETED_WAIT);
        }

        MoveToNextLevel();
    }

    private void SavePercentageOfLevel()
    {
        float
            savedPercentage = PlayerPrefs.GetFloat(LevelName, 0f),
            currentPercentage = _levelManager.GetPercentage();

        if (Inverted)
        {
            currentPercentage = 1 - currentPercentage;
        }

        if (savedPercentage >= currentPercentage)
        {

        }
        else
        {
            PlayerPrefs.SetFloat(LevelName, currentPercentage);
        }

        //ACHIEVEMENT: UPDATE LEVEL PROGRESS
        //ACHIEVEMENT: CHECK IF LEVEL WAS COMPLETED UNDER 5 SECONDS
        //ACHIEVEMENT: CHECK IF LEVEL WAS ACED UNDER 7 SECONDS
        AchievementManager.Instance.CheckForAchievementAtLevelEnd(this, currentPercentage, _timeSinceLevelStart);
    }

    private void MoveToNextLevel()
    {
        _levelManager.FadeLevelOut(_onLevelFadedOutEvent, _shouldUseEvent, false);
    }

    private void ToggleGlobalBlocker(bool toggleState)
    {
        _globalControlBlocker.raycastTarget = toggleState;
    }

    public void ResetTimer()
    {
        _timeSinceLevelStart = 0f;
    }
}
