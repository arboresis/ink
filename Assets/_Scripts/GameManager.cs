using UnityEditor;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static bool _shouldAllowGameControl = true;

    public static bool ShouldAllowGameControl
    {
        get
        {
            return _shouldAllowGameControl;
        }
        set
        {
            GameObject.FindGameObjectWithTag("ButtonController").GetComponent<CanvasGroup>().blocksRaycasts = value;

            _shouldAllowGameControl = value;
        }
    }

    [SerializeField]
    private AudioSource _musicIntroPart;
    [SerializeField]
    private AudioSource _musicLoopPart;

    private void Awake()
    {
        Application.targetFrameRate = 60;

#if UNITY_STANDALONE_WIN
        SetRatio(9, 16);
#endif
    }

    private void Start()
    {
        PlayInitialMusicSequence();
    }

    private void PlayInitialMusicSequence()
    {
        _musicIntroPart.Play();
        _musicLoopPart.PlayScheduled(AudioSettings.dspTime + _musicIntroPart.clip.length);
    }

    void SetRatio(float w, float h)
    {
        if ((((float)Screen.width) / ((float)Screen.height)) > w / h)
        {
            Screen.SetResolution((int)(((float)Screen.height) * (w / h)), Screen.height, true);
        }
        else
        {
            Screen.SetResolution(Screen.width, (int)(((float)Screen.width) * (h / w)), true);
        }
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void RedirectToRateGame()
    {
        Application.OpenURL("market://details?id=" + Application.identifier);
    }

    public void RedirectToPlayStorePage()
    {
        Application.OpenURL("https://play.google.com/store/apps/dev?id=8461501459789094429");
    }
}
