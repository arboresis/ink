using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial : MonoBehaviour
{
    [SerializeField]
    private LevelManager _levelManager;

    [SerializeField]
    private P3dTapThrow _playerThrowController;

    [Space]

    [SerializeField]
    private UnityEvent _onEnableEvent;
    
    [SerializeField]
    private UnityEvent _onDisableEvent;

    [Space]

    [SerializeField]
    private string _skipTutorialAnimationName;

    [SerializeField]
    private string _skipMidTutorialAnimationName;

    [SerializeField]
    private string _skipIntroTutorialAnimationName;

    [SerializeField]
    private GameObject _introPrompt;

    [SerializeField]
    private string _tutorialFigureFirstHitAnimationName;
    private bool _firstHit = false;

    [SerializeField]
    private string _tutorialFinishedAnimationName;

    [SerializeField]
    private GameObject _tutorialNextLevelButton;

    private bool _targetPercentageReached = false;

    [SerializeField]
    private Animator _animator;

    void OnEnable()
    {
        _animator = GetComponent<Animator>();
        _onEnableEvent.Invoke();

        if (PlayerPrefs.GetInt(LevelManager.TUTORIAL_COMPLETED_FLAG, 0) == 1)
        {
            _introPrompt.SetActive(false);
            InterruptPlayerControlWhilePlayingAnimation(_skipIntroTutorialAnimationName);
            Invoke("ResetTutorialFlags", 2f);
        }
    }

    private void OnDisable()
    {
        _onDisableEvent.Invoke();
    }

    private void ResetTutorialFlags()
    {
        _firstHit = false;
        _targetPercentageReached = false;
    }

    public void SkipTutorial()
    {
        StartCoroutine(SkipTutorialToFirstLevel());
    }

    private IEnumerator SkipTutorialToFirstLevel()
    {
        _animator.Play(_skipTutorialAnimationName);

        yield return new WaitUntil(() => _animator.GetCurrentAnimatorStateInfo(0).IsName(_skipTutorialAnimationName));
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);

        FinishTutorial();
    }

    public void SkipMidTutorial()
    {
        StopAllCoroutines();

        _levelManager.ClearBallsAndFingers();

        StartCoroutine(SkipMidTutorialToLastPlayedLevel());
    }

    private IEnumerator SkipMidTutorialToLastPlayedLevel()
    {
        _animator.Play(_skipMidTutorialAnimationName);

        yield return new WaitUntil(() => _animator.GetCurrentAnimatorStateInfo(0).IsName(_skipMidTutorialAnimationName));
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);

        FinishTutorial(true);
    }

    public void FinishTutorial(bool shouldLoadLastPlayedLevel = false)
    {
        if (shouldLoadLastPlayedLevel == false)
        {
            _levelManager.FinishTutorial();
        }
        else
        {
            _levelManager.FinishTutorialAndResume();
        }
    }

    public void InterruptPlayerControlWhilePlayingAnimation(string animationName)
    {
        StartCoroutine(PlayAnimationWithoutPlayerControl(animationName));
    }

    private IEnumerator PlayAnimationWithoutPlayerControl(string animationName)
    {
        _playerThrowController.enabled = false;

        _animator.Play(animationName);

        yield return new WaitUntil(() => _animator.GetCurrentAnimatorStateInfo(0).IsName(animationName));
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);

        _playerThrowController.enabled = true;

        if (animationName == _tutorialFinishedAnimationName)
        {
            FinishTutorial();
        }
    }

    public void OnTutorialFigureFirstHit()
    {
        if (_firstHit == false)
        {
            _animator.Play(_tutorialFigureFirstHitAnimationName);
            _firstHit = true;
        }
    }

    public void OnTutorialLevelPercentageReached()
    {
        if (_targetPercentageReached == false)
        {
            _tutorialNextLevelButton.SetActive(true);
            _targetPercentageReached = true;
        }
    }
}
