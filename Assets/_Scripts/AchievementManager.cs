using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gley.GameServices;

public class AchievementManager : MonoBehaviour
{
    public static AchievementManager Instance
    {
        get
        {
            return _Instance;
        }
    }

    private static AchievementManager _Instance;

    [SerializeField]
    private LevelManager _levelManager;

    private List<Level> _levelList;

    private string _lastCategoryName;

    private Dictionary<string, List<Level>> _categoryLevelListPairs = new Dictionary<string, List<Level>>();

    [Space]
    [SerializeField]
    private float _minimumCompleteTimeAchievement = 10f;

    [SerializeField]
    private float _minimumAceTimeAchievement = 10f;

    private void Awake()
    {
        _Instance = this;

        API.LogIn();
    }

    // Start is called before the first frame update
    void Start()
    {
        _levelList = new List<Level>();
        _levelList = _levelManager.GetLevelList();

        for (int i = 0; i < _levelList.Count; i++)
        {
            if (string.IsNullOrWhiteSpace(_levelList[i].CategoryName))
            {
                continue;
            }

            if (_levelList[i].CategoryName != _lastCategoryName)
            {
                _lastCategoryName = _levelList[i].CategoryName;

                _categoryLevelListPairs.Add(_lastCategoryName, new List<Level>());
            }

            _categoryLevelListPairs.GetValueOrDefault(_lastCategoryName).Add(_levelList[i]);
        }

        foreach (KeyValuePair<string, List<Level>> keyValuePair in _categoryLevelListPairs)
        {
            string result = "Category levels: \n";
            foreach (var item in keyValuePair.Value)
            {
                result += item.ToString() + ",\n";
            }

            Debug.Log("Achievement Manager has found a category: " + keyValuePair.Key + "\n" + result);
        }
    }

    public void CheckForAchievementAtLevelEnd(Level completedLevel, float percentage, float timeSinceLevelStart)
    {
        if (string.IsNullOrWhiteSpace(completedLevel.CategoryName) == false)
        {
            List<Level> categoryLevels = _categoryLevelListPairs.GetValueOrDefault(completedLevel.CategoryName);

            int amountOfLevels = categoryLevels.Count;
            int amountOfLevelsCompleted = 0;
            int amountOfLevelsAced = 0;

            foreach (Level level in categoryLevels)
            {
                float savedPercentage = PlayerPrefs.GetFloat(level.LevelName, 0f);

                if (savedPercentage > 0f)
                {
                    amountOfLevelsCompleted++;
                }

                if (savedPercentage == 1f)
                {
                    amountOfLevelsAced++;
                }
            }

            if (amountOfLevelsCompleted == amountOfLevels)
            {
                SubmitAchievement(GetCompletedCategoryAchievementName(completedLevel.CategoryName));
                Debug.Log("All levels in category " + completedLevel.CategoryName + " have been completed!");
            }

            if (amountOfLevelsAced == amountOfLevels && percentage == 1f)
            {
                SubmitAchievement(GetAcedCategoryAchievementName(completedLevel.CategoryName));
                Debug.Log("All levels in category " + completedLevel.CategoryName + " have been aced!");
            }

            if (timeSinceLevelStart <= _minimumCompleteTimeAchievement)
            {
                SubmitAchievement(GPGSIds.achievement_in_a_rush);
                Debug.Log("Completed level in under " + _minimumCompleteTimeAchievement);
            }

            if (percentage == 1f && timeSinceLevelStart <= _minimumAceTimeAchievement)
            {
                SubmitAchievement(GPGSIds.achievement_is_that_all_youve_got);
                Debug.Log("Aced level in under " + _minimumAceTimeAchievement);
            }
        }

        int furthestLevel = PlayerPrefs.GetInt("FurthestLevel", 0);
        int lastLevelIndex = (_levelManager.GetLevelList().Count - 1) - 4;

        
        int levelCount = lastLevelIndex + 1;
        int globalAmountOfLevelsCompleted = 0;
        int globalAmountOfLevelsAced = 0;

        foreach (Level level in _levelManager.GetLevelList())
        {
            if (PlayerPrefs.GetFloat(level.LevelName, 0f) > 0f)
            {
                globalAmountOfLevelsCompleted++;
            }

            if (PlayerPrefs.GetFloat(level.LevelName, 0f) == 1f)
            {
                globalAmountOfLevelsAced++;
            }
        }

        Debug.LogWarning("Amount of levels completed: " + globalAmountOfLevelsCompleted + "\nAmount of levels aced: " + globalAmountOfLevelsAced);
    }

    public void SubmitAchievement(string achievementID)
    {
        Social.ReportProgress(achievementID, 100f, SubmitAchievementSuccessfulCallback);
    }

    public void SubmitAchievementSuccessfulCallback(bool successState)
    {
        Debug.Log("Successfully submitted progress towards achievement!");
    }

    public void ShowAchievementsUI()
    {
        Social.ShowAchievementsUI();
    }

    private string GetCompletedCategoryAchievementName(string categoryName)
    {
        switch (categoryName)
        {
            case "Basic Shapes":
                return GPGSIds.achievement_babys_first_steps;
            case "Origami Animals":
                return GPGSIds.achievement_the_unfinished_swan;
            case "Work Tools":
                return GPGSIds.achievement_an_injured_foreman;
            case "Furniture":
                return GPGSIds.achievement_cozy_lazy_lovely_afternoon;
            case "Household Items":
                return GPGSIds.achievement_all_in_the_same_room;
            case "Transport Methods":
                return GPGSIds.achievement_to_walk_or_not_to_walk;
            case "Clothing Items":
                return GPGSIds.achievement_going_out;
            case "Animals":
                return GPGSIds.achievement_zoo_keeper;
            case "Instruments":
                return GPGSIds.achievement_crescendo;
            case "Sports":
                return GPGSIds.achievement_healthy_as_a_horse;
            case "Flowers":
                return GPGSIds.achievement_floating_on_a_gentle_breeze;
            case "Birds":
                return GPGSIds.achievement_space_migrants;
            case "Continents":
                return GPGSIds.achievement_so_this_is_where_the_birds_went;
            case "Locations":
                return GPGSIds.achievement_were_gonna_need_a_bigger_map;
            case "Albums":
                return GPGSIds.achievement_breaking_through_the_wall;
            case "Paintings":
                return GPGSIds.achievement_happy_little_accidents;
            default:
                return default;
        }
    }

    private string GetAcedCategoryAchievementName(string categoryName)
    {
        switch (categoryName)
        {
            case "Basic Shapes":
                return GPGSIds.achievement_the_geometry_master;
            case "Origami Animals":
                return GPGSIds.achievement_the_heavy_rainer;
            case "Work Tools":
                return GPGSIds.achievement_the_handyperson;
            case "Furniture":
                return GPGSIds.achievement_the_ikea_catalog;
            case "Household Items":
                return GPGSIds.achievement_the_interior_decorator;
            case "Transport Methods":
                return GPGSIds.achievement_luke_nonwalker;
            case "Clothing Items":
                return GPGSIds.achievement_going_out_2;
            case "Animals":
                return GPGSIds.achievement_the_exquisite_farmer;
            case "Instruments":
                return GPGSIds.achievement_the_living_beethoven;
            case "Sports":
                return GPGSIds.achievement_the_allrounder;
            case "Flowers":
                return GPGSIds.achievement_the_gardener;
            case "Birds":
                return GPGSIds.achievement_a_fellow_bird_enthusiast;
            case "Continents":
                return GPGSIds.achievement_citizen_of_the_world;
            case "Locations":
                return GPGSIds.achievement_the_geoguessr;
            case "Albums":
                return GPGSIds.achievement_ink_4_world_tour;
            case "Paintings":
                return GPGSIds.achievement_no_dead_flowers_here;
            default:
                return default;
        }
    }

    public void ResetLevelPercentages()
    {
        foreach (Level level in _levelManager.GetLevelList())
        {
            PlayerPrefs.SetFloat(level.LevelName, 0f);
        }

        PlayerPrefs.DeleteKey("LastPlayedLevel");
        PlayerPrefs.DeleteKey("FurthestLevel");
    }

    public void ResetAchievements()
    {
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.BasicShapesCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.BasicShapesAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.OrigamiAnimalsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.OrigamiAnimalsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.WorkToolsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.WorkToolsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.FurnitureCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.FurnitureAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.HouseholdItemsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.HouseholdItemsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.TransportMethodsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.TransportMethodsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.ClothingItemsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.ClothingItemsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.AnimalsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.AnimalsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.InstrumentsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.InstrumentsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.SportsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.SportsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.FlowersCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.FlowersAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.BirdsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.BirdsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.ContinentsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.ContinentsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.LocationsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.LocationsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.AlbumsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.AlbumsAced, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.PaintingsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.PaintingsAced, 0);

        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.AllLevelsCompleted, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.AllLevelsAced, 0);

        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.CompleteLevelUnderXTime, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.AceLevelUnderXTime, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.ExplodeTwinBall, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.ExplodeSmallerBall, 0);
        PlayerPrefs.SetInt("AchievementUnlocked:" + AchievementNames.ExplodeBiggerBall, 0);
    }

    public class AchievementNames
    {
        public static string
            BasicShapesCompleted = "Baby's First Steps",
            BasicShapesAced = "The Geometry Master",
            OrigamiAnimalsCompleted = "The Unfinished Swan",
            OrigamiAnimalsAced = "The Heavy Rainer",
            WorkToolsCompleted = "An Injured Foreman",
            WorkToolsAced = "The Handyperson",
            FurnitureCompleted = "Cozy, Lazy, Lovely Afternoon",
            FurnitureAced = "The IKEA Catalog",
            HouseholdItemsCompleted = "All in the Same Room?",
            HouseholdItemsAced = "The Interior Decorator",
            TransportMethodsCompleted = "To Walk or Not to Walk...",
            TransportMethodsAced = "Luke Nonwalker",
            ClothingItemsCompleted = "Going Out?",
            ClothingItemsAced = "Going Out!",
            AnimalsCompleted = "Zoo Keeper",
            AnimalsAced = "The Exquisite Farmer",
            InstrumentsCompleted = "Crescendo!",
            InstrumentsAced = "The (Living) Beethoven",
            SportsCompleted = "Healthy as a Horse",
            SportsAced = "The All-Rounder",
            FlowersCompleted = "Floating on a Gentle Breeze",
            FlowersAced = "The Gardener",
            BirdsCompleted = "Space Migrants",
            BirdsAced = "A Fellow Bird Enthusiast",
            ContinentsCompleted = "So This is Where the Birds Went...",
            ContinentsAced = "Citizen of the World",
            LocationsCompleted = "We're Gonna Need a Bigger Map",
            LocationsAced = "The Geoguessr",
            AlbumsCompleted = "Breaking Through 'The Wall'",
            AlbumsAced = "Ink 4: World Tour",
            PaintingsCompleted = "Happy Little Accidents",
            PaintingsAced = "No Dead Flowers Here",

            AllLevelsCompleted = "They Grow Up So Fast",
            AllLevelsAced = "The Ace of Spades",

            CompleteLevelUnderXTime = "In a Rush",
            AceLevelUnderXTime = "Is That All You've Got?",
            ExplodeTwinBall = "Jinx, soulmate!",
            ExplodeSmallerBall = "Golias",
            ExplodeBiggerBall = "David";
    }
}