using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollbarResetOnEnable : MonoBehaviour
{
    private Scrollbar _scrollbar;

    void Awake()
    {
        _scrollbar = GetComponent<Scrollbar>();
    }

    void OnEnable()
    {
        StartCoroutine(ResetScrollbar());
    }

    private IEnumerator ResetScrollbar()
    {
        yield return new WaitForFixedUpdate();

        _scrollbar.value = 1;
    }
}
