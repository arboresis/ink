using UnityEngine;

public class DebugElement : MonoBehaviour
{
    [SerializeField]
    private bool _unityEditor = true;

    [SerializeField]
    private bool _debugBuild = true;

    private void Awake()
    {
        //If this is a production build
        if(Application.isEditor == false && Debug.isDebugBuild == false)
        {
            Destroy(gameObject);
        }

        if(Application.isEditor)
        {
            if(_unityEditor)
            {
                return;
            }

            Destroy(gameObject);
        }

        if (Debug.isDebugBuild)
        {
            if (_debugBuild)
            {
                return;
            }

            Destroy(gameObject);
        }
    }
}
