using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorResetOnEnable : MonoBehaviour
{
    private Animator _animator;

    [SerializeField]
    private string _stateNameToResetTo;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        _animator.Play(_stateNameToResetTo);
    }
}
