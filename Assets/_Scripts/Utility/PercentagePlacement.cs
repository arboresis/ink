using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PercentagePlacement : MonoBehaviour
{
    [SerializeField]
    private float
        _horizontalPercentage = 0f,
        _verticalPercentage = 0f;

    void Awake()
    {
        var v3Pos = new Vector3(_horizontalPercentage / 100f, _verticalPercentage / 100f, 10.05f);
        transform.position = Camera.main.ViewportToWorldPoint(v3Pos);
    }
}
