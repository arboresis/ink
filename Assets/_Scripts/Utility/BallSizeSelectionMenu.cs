using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallSizeSelectionMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject _ballSizeMenu;

    [Space]
    [SerializeField]
    private LevelManager _levelManager;

    [Space]
    [SerializeField] private GameObject _smallBallIcon;
    [SerializeField] private GameObject _mediumBallIcon;
    [SerializeField] private GameObject _bigBallIcon;
    [SerializeField] private GameObject _nukeBallIcon;

    [Space]
    [SerializeField] private Button _smallBallButton;
    [SerializeField] private Button _mediumBallButton;
    [SerializeField] private Button _bigBallButton;
    [SerializeField] private Button _nukeBallButton;

    [SerializeField]
    private RewardedAdsButton _nukeRewardAdButton;

    [SerializeField]
    private Button _menuButton;

    [SerializeField]
    private Button _outOfBallsTextWarning;

    private Coroutine _outOfBallsAnimationCoroutine;

    public void ToggleBallSizeMenu()
    {
        _ballSizeMenu.SetActive(!_ballSizeMenu.activeInHierarchy);

        //If the ball size selection menu is now open, check each type of ball and deactivate ball types that are tapped out
        if(_ballSizeMenu.activeInHierarchy)
        {
            _smallBallButton.interactable = _levelManager.HasInkBallsOfType(BallType.Small);
            _mediumBallButton.interactable = _levelManager.HasInkBallsOfType(BallType.Medium);
            _bigBallButton.interactable = _levelManager.HasInkBallsOfType(BallType.Big);
            _nukeBallButton.interactable = _levelManager.HasInkBallsOfType(BallType.Nuke);

            if (_nukeRewardAdButton.GetRewardedAdsButton().interactable == false && _nukeRewardAdButton.LoadingNewAd == false)
            {
                _nukeRewardAdButton.LoadAd();
            }
        }
    }

    public void HideBallSizeMenu()
    {
        _ballSizeMenu.SetActive(false);
    }

    public void ShowBallType(BallType ballType)
    {
        switch (ballType)
        {
            case BallType.Small:
                _smallBallIcon.SetActive(true);
                _mediumBallIcon.SetActive(false);
                _bigBallIcon.SetActive(false);
                _nukeBallIcon.SetActive(false);
                break;
            case BallType.Medium:
                _smallBallIcon.SetActive(false);
                _mediumBallIcon.SetActive(true);
                _bigBallIcon.SetActive(false);
                _nukeBallIcon.SetActive(false);
                break;
            case BallType.Big:
                _smallBallIcon.SetActive(false);
                _mediumBallIcon.SetActive(false);
                _bigBallIcon.SetActive(true);
                _nukeBallIcon.SetActive(false);
                break;
            case BallType.Nuke:
                _smallBallIcon.SetActive(false);
                _mediumBallIcon.SetActive(false);
                _bigBallIcon.SetActive(false);
                _nukeBallIcon.SetActive(true);
                break;
        }
    }

    public void PlayNoBallsAnimation()
    {
        if(_outOfBallsAnimationCoroutine != null)
        {
            StopCoroutine(_outOfBallsAnimationCoroutine);
        }

        _outOfBallsAnimationCoroutine = StartCoroutine(PlayOutOfBallsAnimation());
    }

    private IEnumerator PlayOutOfBallsAnimation()
    {
        _menuButton.interactable = true;
        _outOfBallsTextWarning.interactable = true;

        yield return new WaitForSeconds(_menuButton.colors.fadeDuration);

        _menuButton.interactable = false;

        yield return new WaitForSeconds(_menuButton.colors.fadeDuration);

        _menuButton.interactable = true;

        yield return new WaitForSeconds(_menuButton.colors.fadeDuration);

        _menuButton.interactable = false;

        yield return new WaitForSeconds(_menuButton.colors.fadeDuration);

        _menuButton.interactable = true;

        yield return new WaitForSeconds(_outOfBallsTextWarning.colors.fadeDuration * 2f);

        _outOfBallsTextWarning.interactable = false;
    }
}
