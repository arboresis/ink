using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ToggleEventWhenTrue : MonoBehaviour
{
    public UnityEvent ToggleIsTrueEvent;

    void Awake()
    {
        GetComponent<Toggle>().onValueChanged.AddListener(ToggleIsTrue);
    }

    private void ToggleIsTrue(bool isOn)
    {
        if (isOn)
        {
            ToggleIsTrueEvent.Invoke();
        }
    }
}
