using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuGameController : MonoBehaviour
{
    public void DisallowGameControl()
    {
        GameManager.ShouldAllowGameControl = false;
    }

    public void AllowGameControl()
    {
        GameManager.ShouldAllowGameControl = true;
    }
}
