using PaintIn3D;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteAlways]
public class SetLineRendererSecondPointPosition : MonoBehaviour
{
    private LineRenderer _lineRenderer;

    [SerializeField]
    private Vector3 _firstPointPosition;

    [SerializeField]
    private Vector3 _secondPointPosition;
    
    void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        _lineRenderer.SetPosition(0, _firstPointPosition);
        _lineRenderer.SetPosition(1, _secondPointPosition);
    }
}