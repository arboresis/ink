using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AndroidBackButtonListener : MonoBehaviour
{
    private Button _button;

    [SerializeField]
    private CanvasGroup _canvasGroup;

    void Awake()
    {
        _button = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (_button.interactable && _canvasGroup.interactable && _canvasGroup.blocksRaycasts)
            {
                _button.onClick.Invoke();
            }
        }
    }
}
