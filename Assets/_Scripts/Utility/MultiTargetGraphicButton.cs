﻿using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A button that has multiple target graphics. Useful for buttons with text.
/// </summary>
public class MultiTargetGraphicButton : Button
{
    private Graphic[] graphics;
    protected Graphic[] targetGraphics
    {
        get
        {
            if (graphics == null)
            {
                graphics = targetGraphic.transform.GetComponentsInChildren<Graphic>();
            }
            return graphics;
        }
    }

    /// <summary>
    /// Change the button's current state to another one.
    /// </summary>
    /// <param name="state">The state to which this button will transition.</param>
    /// <param name="instant">A bool to check if the transition is instant.</param>
    protected override void DoStateTransition(SelectionState state, bool instant)
    {
        Color targetColor;
        switch (state)
        {
            case SelectionState.Normal:
                targetColor = colors.normalColor;
                break;
            case SelectionState.Highlighted:
                targetColor = colors.highlightedColor;
                break;
            case SelectionState.Pressed:
                targetColor = colors.pressedColor;
                break;
            case SelectionState.Disabled:
                targetColor = colors.disabledColor;
                break;
            default:
                targetColor = Color.black;
                break;
        }

        if (gameObject.activeInHierarchy)
        {
            if(transition == Transition.ColorTint)
            {
                ColorTween(targetColor * colors.colorMultiplier, instant);
            }
        }
    }

    /// <summary>
    /// Crossfade the button's color between the current state's color and the target state's color.
    /// </summary>
    /// <param name="targetColor">The target color for the new button state.</param>
    /// <param name="instant">A bool to check if the transition is instant.</param>
    private void ColorTween(Color targetColor, bool instant)
    {
        if (targetGraphic == null)
        {
            return;
        }

        foreach (Graphic g in targetGraphics)
        {
            g.CrossFadeColor(targetColor, (!instant) ? this.colors.fadeDuration : 0f, true, true);
        }
    }
}
