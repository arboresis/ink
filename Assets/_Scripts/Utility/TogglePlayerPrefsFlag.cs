using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TogglePlayerPrefsFlag : MonoBehaviour
{
    private Toggle _toggle;

    [SerializeField]
    private string _settingName;

    [Space]
    [SerializeField]
    private int _toggleOffValue = 0;

    [SerializeField]
    private int _toggleOnValue = 1;

    [Space]
    [SerializeField]
    private bool _shouldResetLevel = false;

    [SerializeField]
    private LevelManager _levelManager;

    private void Awake()
    {
        _toggle = GetComponent<Toggle>();
        _toggle.onValueChanged.AddListener(OnValueChanged);
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        _toggle.isOn = (PlayerPrefs.GetInt(_settingName, _toggleOffValue) == _toggleOnValue);
    }

    private void OnDestroy()
    {
        _toggle.onValueChanged.RemoveListener(OnValueChanged);
    }

    private void OnValueChanged(bool value)
    {
        if(value == true)
        {
            PlayerPrefs.SetInt(_settingName, _toggleOnValue);
        }
        else
        {
            PlayerPrefs.SetInt(_settingName, _toggleOffValue);
        }

        if (_shouldResetLevel)
        {
            _levelManager.ResetCurrentLevel();
        }
    }
}
