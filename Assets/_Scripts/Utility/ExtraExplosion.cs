using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraExplosion : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _particleSystem;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PlayExtraExplosion());
    }

    private IEnumerator PlayExtraExplosion()
    {
        if (CheatsManager.HasInktinArboresitinoEnabled() == false)
        {
            Destroy(gameObject);
            yield break;
        }

        transform.SetParent(null);

        _particleSystem.Play();
        yield return new WaitForSeconds(_particleSystem.main.duration);

        Destroy(gameObject);
    }
}
