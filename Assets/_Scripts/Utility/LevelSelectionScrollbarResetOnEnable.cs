using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionScrollbarResetOnEnable : MonoBehaviour
{
    private Scrollbar _scrollbar;

    [SerializeField]
    private LevelSelectionMenuManager _levelSelectionManager;

    [SerializeField]
    private ScrollRect scrollRect;

    [SerializeField]
    private RectTransform contentPanel;

    [SerializeField]
    private float autoScrollOffset;

    void Awake()
    {
        _scrollbar = GetComponent<Scrollbar>();
    }

    void OnEnable()
    {
        StartCoroutine(ResetScrollbar());
    }

    private IEnumerator ResetScrollbar()
    {
        yield return new WaitForFixedUpdate();

        SnapTo(_levelSelectionManager.GetActiveButtonTransform());
    }

    public void SnapTo(RectTransform target)
    {
        Canvas.ForceUpdateCanvases();

        contentPanel.anchoredPosition =
            new Vector2(contentPanel.anchoredPosition.x,
                ((Vector2)scrollRect.transform.InverseTransformPoint(contentPanel.position)
                - (Vector2)scrollRect.transform.InverseTransformPoint(target.position)).y + autoScrollOffset);
    }
}
