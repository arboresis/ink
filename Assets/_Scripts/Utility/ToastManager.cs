using UnityEngine;

public class ToastManager : MonoBehaviour
{
    public static ToastManager Instance
    {
        get
        {
            return _Instance;
        }
    }

    private static ToastManager _Instance;

    AndroidJavaObject currentActivity;

    private void Awake()
    {
        _Instance = this;
    }

    public void Start()
    {
#if !UNITY_STANDALONE_WIN && !UNITY_EDITOR
        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
#endif
    }

    public void CreateToast(string message, bool isLongToast)
    {
#if !UNITY_STANDALONE_WIN && !UNITY_EDITOR
        string toastLength;

        if (isLongToast)
        {
            toastLength = "LENGTH_LONG";
        }
        else
        {
            toastLength = "LENGTH_SHORT";
        }

        AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
        AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
        AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", message);
        AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>(toastLength));
        toast.Call("show");
#endif
    }

}