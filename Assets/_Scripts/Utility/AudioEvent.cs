using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioEvent : MonoBehaviour
{
    public void StartPlayingSound(AudioClip audioClip, bool loopSound, AudioMixerGroup mixerGroup, float volume = 1f)
    {
        AudioSource audioSource = GetComponent<AudioSource>();

        audioSource.clip = audioClip;
        audioSource.loop = loopSound;
        audioSource.volume = volume;
        audioSource.outputAudioMixerGroup = mixerGroup;

        audioSource.Play();

        if(loopSound == false)
        {
            StartCoroutine(DestroyWhenFinished(audioClip.length + 0.05f));
        }
    }

    private IEnumerator DestroyWhenFinished(float destroyDelay)
    {
        yield return new WaitForSeconds(destroyDelay);
        Destroy(gameObject);
    }
}
