using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using Random = UnityEngine.Random;

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class ProVersionPurchaser : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    // Product identifiers for all products capable of being purchased: 
    // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
    // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
    // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    // General product identifiers for the consumable, non-consumable, and subscription products.
    // Use these handles in the code to reference which product to purchase. Also use these values 
    // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
    // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
    // specific mapping to Unity Purchasing's AddProduct, below.
    public static string kProductIDNonConsumable = "inkproversion";

    public static bool HasProVersion
    {
        get
        {
            _hasProVersion = PlayerPrefs.GetString("THWAU") == GetPurchaseCode();
            return _hasProVersion;
        }

        set
        {
            _hasProVersion = value;

            if (value == false)
            {
                _doesntHaveProVersionEvent.Invoke();
            }
            else
            {
                _hasProVersionEvent.Invoke();
                Advertisement.Banner.Hide(true);
            }
        }
    }

    private static bool _hasProVersion = false;

    private static string GetPurchaseCode()
    {
        string aBBqu5a = (Random.Range(0f, 20f) + "2!3$$" + Random.Range(0f, 20f)).ToString();

        return GetPurchaseCode(aBBqu5a);
    }

    private static string GetPurchaseCode(string a5uqBBa)
    {
        return SystemInfo.deviceUniqueIdentifier.Substring(((SystemInfo.deviceUniqueIdentifier.Length - 1) / 3) * 2 - 5, (SystemInfo.deviceUniqueIdentifier.Length - 1) / 3) + "&o" + SystemInfo.deviceUniqueIdentifier.Substring(((SystemInfo.deviceUniqueIdentifier.Length - 1) / 3) - 3, (SystemInfo.deviceUniqueIdentifier.Length - 1) / 3) + "oP" + SystemInfo.deviceUniqueIdentifier.Substring(0, (SystemInfo.deviceUniqueIdentifier.Length - 1) / 3);
    }

    private static UnityEvent _hasProVersionEvent;
    private static UnityEvent _doesntHaveProVersionEvent;

    public UnityEvent HasProVersionEvent;
    public UnityEvent DoesntHaveProVersionEvent;
    public UnityEvent IsWindowsVersionEvent;

    private void Awake()
    {
        _hasProVersionEvent = HasProVersionEvent;
        _doesntHaveProVersionEvent = DoesntHaveProVersionEvent;

        _doesntHaveProVersionEvent.Invoke();
    }

    void Start()
    {
#if UNITY_STANDALONE_WINDOWS
        HasProVersion = true;
        IsWindowsVersionEvent.Invoke();
#else
        if (HasProVersion)
        {
            HasProVersion = true;
            return;
        }

        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
#endif
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable);

        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    public void BuyNonConsumable()
    {
        // Buy the non-consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID(kProductIDNonConsumable);
    }


    void BuyProductID(string productId)
    {
        if (Application.isEditor || Debug.isDebugBuild)
        {
            THWAU();
            HasProVersion = true;
            return;
        }

        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }


    //  
    // --- IStoreListener
    //

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;

        ProductCollection prod = m_StoreController.products;

        foreach (Product product in prod.all)
        {
            Debug.LogWarning(product.metadata.localizedTitle + "\n" + product.hasReceipt);

            if (String.Equals(product.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
            {
                continue;
            }

            HasProVersion = product.hasReceipt;

            if (product.hasReceipt)
            {
                THWAU();
            }
        }
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public void OnInitializeFailed(InitializationFailureReason error, string message)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        // Or ... a non-consumable product has been purchased by this user.
        if (String.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
            THWAU();
            HasProVersion = true;
        }
        // Or ... an unknown product has been purchased by this user. Fill in additional products here....
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }

    private void THWAU()
    {
        PlayerPrefs.SetString("THWAU", GetPurchaseCode());
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        HasProVersion = false;
    }
}