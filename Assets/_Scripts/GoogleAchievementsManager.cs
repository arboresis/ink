using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class GoogleAchievementsManager : MonoBehaviour
{
    public static GoogleAchievementsManager Instance
    {
        get
        {
            return _Instance;
        }
    }

    private static GoogleAchievementsManager _Instance;

    private bool _connectedToPlayStore;

    void Awake()
    {
        _Instance = this;

        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
    }

    private void Start()
    {
        LogInToGooglePlay();
    }

    private void LogInToGooglePlay(bool forceManual = false)
    {
        if (forceManual == false)
        {
            PlayGamesPlatform.Instance.Authenticate(ProcessAuthentication);
        }
        else
        {
            PlayGamesPlatform.Instance.ManuallyAuthenticate(ProcessAuthentication);
        }
    }

    private void ProcessAuthentication(SignInStatus status)
    {
        if (status == SignInStatus.Success)
        {
            _connectedToPlayStore = true;
        }
        else
        {
            _connectedToPlayStore = false;
        }
    }

    public void LeaderboardUpdateResult(bool result)
    {
        if (result == false)
        {
            Debug.LogWarning("Unable to update score for the Google Play leaderboard!");
        }
        else
        {
            Debug.Log("Score successfully updated on Google Play leaderboard.");
        }
    }

    public void ShowLeaderboardUI()
    {
        if (_connectedToPlayStore == false)
        {
            LogInToGooglePlay(forceManual: true);
        }

        PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkI0NXKv5keEAIQAQ", LeaderboardTimeSpan.Weekly, null);
    }
}
