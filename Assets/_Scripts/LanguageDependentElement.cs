using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LanguageDependentElement : MonoBehaviour
{
    [SerializeField, TextArea]
    protected string
        _English,
        _French,
        _German,
        _Italian,
        _Polish,
        _Portuguese,
        _Russian,
        _SimplifiedChinese,
        _Spanish;

    [SerializeField]
    private TextMeshProUGUI _textElement;

    // Start is called before the first frame update
    void Awake()
    {
        if (_textElement == null)
        {
            _textElement = GetComponent<TextMeshProUGUI>();
        }
    }

    // Update is called once per frame
    public void UpdateElement(Language language)
    {
        if(_textElement == null)
        {
            _textElement = GetComponent<TextMeshProUGUI>();
        }

        switch (language)
        {
            case Language.English:
                _textElement.text = _English;
                break;
            case Language.French:
                _textElement.text = _French;
                break;
            case Language.German:
                _textElement.text = _German;
                break;
            case Language.Italian:
                _textElement.text = _Italian;
                break;
            case Language.Polish:
                _textElement.text = _Polish;
                break;
            case Language.Portuguese:
                _textElement.text = _Portuguese;
                break;
            case Language.Russian:
                _textElement.text = _Russian;
                break;
            case Language.SimplifiedChinese:
                _textElement.text = _SimplifiedChinese;
                break;
            case Language.Spanish:
                _textElement.text = _Spanish;
                break;
        }
    }
}
