using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatsManager : MonoBehaviour
{
    public class CheatsNames
    {
        public const string InfiniteBalls = "InfiniteBalls";
        public const string MoonGravity = "MoonGravity";
        public const string SillySoundEffects = "SillySoundEffects";
        public const string InktinArboresitino = "InktinArboresitino";
    }

    public const float MoonGravityMultiplier = 0.16531f;

    private static bool _hasProVersion = false;

    public static bool HasInfiniteBallsEnabled()
    {
        return PlayerPrefs.GetInt(CheatsNames.InfiniteBalls, 0) == 1 && _hasProVersion;
    }

    public static bool HasMoonGravityEnabled()
    {
        return PlayerPrefs.GetInt(CheatsNames.MoonGravity, 0) == 1 && _hasProVersion;
    }

    public static bool HasSillySoundEffectsEnabled()
    {
        return PlayerPrefs.GetInt(CheatsNames.SillySoundEffects, 0) == 1 && _hasProVersion;
    }

    public static bool HasInktinArboresitinoEnabled()
    {
        return PlayerPrefs.GetInt(CheatsNames.InktinArboresitino, 0) == 1 && _hasProVersion;
    }

    public void ResetCheats()
    {
        PlayerPrefs.SetString("THWAU", "");
    }

    public void DisableAchievements()
    {
        _hasProVersion = true;
    }
}