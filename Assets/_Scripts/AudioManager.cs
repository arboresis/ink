using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public enum AudioEventType
{
    Music,
    SoundEffect
}

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance
    {
        get
        {
            return _Instance;
        }
    }

    private static AudioManager _Instance;

    [SerializeField]
    private GameObject _audioEventPrefab;

    [Space]
    [SerializeField] private AudioMixerGroup _masterMixerGroup;
    [SerializeField] private AudioMixerGroup _musicMixerGroup;
    [SerializeField] private AudioMixerGroup _soundFXMixerGroup;

    [Space]
    [SerializeField] private Slider _masterVolumeSlider;
    [SerializeField] private Slider _musicVolumeSlider;
    [SerializeField] private Slider _soundFXVolumeSlider;

    private void Awake()
    {
        _Instance = this;
    }

    private void Start()
    {
        ChangeSliderValue(_masterVolumeSlider, "MasterVolume");
        ChangeSliderValue(_musicVolumeSlider, "MusicVolume");
        ChangeSliderValue(_soundFXVolumeSlider, "SoundEffectsVolume");
    }

    public AudioEvent PlaySound(AudioClip audioClip, bool loopSound, AudioEventType type, float volume = 1f)
    {
        AudioEvent audioEvent = Instantiate(_audioEventPrefab).GetComponent<AudioEvent>();

        audioEvent.StartPlayingSound(audioClip, loopSound, GetMixerGroupForType(type), volume);

        return audioEvent;
    }

    private AudioMixerGroup GetMixerGroupForType(AudioEventType type)
    {
        switch (type)
        {
            case AudioEventType.Music:
                return _musicMixerGroup;
            case AudioEventType.SoundEffect:
                return _soundFXMixerGroup;
            default:
                return _soundFXMixerGroup;
        }
    }

    public void ChangeSliderValue(Slider slider, string settingName)
    {
        slider.value = PlayerPrefs.GetFloat(settingName, 100f);
    }

    public void ChangeMasterVolume(float value)
    {
        float dBvalue = LinearToDecibel(value);

        _masterMixerGroup.audioMixer.SetFloat("MasterVolume", dBvalue);
        PlayerPrefs.SetFloat("MasterVolume", value);
        PlayerPrefs.Save();
    }

    public void ChangeMusicVolume(float value)
    {
        float dBvalue = LinearToDecibel(value);

        _masterMixerGroup.audioMixer.SetFloat("MusicVolume", dBvalue);
        PlayerPrefs.SetFloat("MusicVolume", value);
        PlayerPrefs.Save();
    }

    public void ChangeSoundEffectsVolume(float value)
    {
        float dBvalue = LinearToDecibel(value);

        _masterMixerGroup.audioMixer.SetFloat("SoundEffectsVolume", dBvalue);
        PlayerPrefs.SetFloat("SoundEffectsVolume", value);
        PlayerPrefs.Save();
    }

    private float LinearToDecibel(float linear)
    {
        float dB;

        if (linear != 0)
            dB = (40.0f * Mathf.Log10(linear)) - 80f;
        else
            dB = -144.0f;

        return dB;
    }
}
