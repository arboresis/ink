using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Collections;

public class RewardedAdsButton : MonoBehaviour, IUnityAdsLoadListener, IUnityAdsShowListener
{
    [SerializeField] Button _showAdButton;
    [SerializeField] string _androidAdUnitId = "Rewarded_Android";
    string _adUnitId;

    [SerializeField]
    private LevelManager _levelManager;

    [HideInInspector]
    public bool LoadingNewAd = false;

    [Space]

    [SerializeField]
    string _failedToLoadMessage = "";
    [SerializeField]
    string _failedToShowMessage = "";

    void Awake()
    {
        // Get the Ad Unit ID for the current platform:
        _adUnitId = _androidAdUnitId;

        // Configure the button to call the ShowAd() method when clicked:
        _showAdButton.onClick.AddListener(LoadAd);
    }

    public Button GetRewardedAdsButton()
    {
        return _showAdButton;
    }

    // Load content to the Ad Unit:
    public void LoadAd()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            StartCoroutine(ShowToast(_failedToLoadMessage, true));
            return;
        }

        // IMPORTANT! Only load content AFTER initialization (in this example, initialization is handled in a different script).
        Debug.LogWarning("LoadAd");
        LoadingNewAd = true;
        Advertisement.Load(_adUnitId, this);
    }

    // If the ad successfully loads, add a listener to the button and enable it:
    public void OnUnityAdsAdLoaded(string adUnitId)
    {
        Debug.LogWarning("OnUnityAdsAdLoaded");

        if (adUnitId.Equals(_adUnitId))
        {
            LoadingNewAd = false;

            ShowAd();
        }
    }

    // Implement a method to execute when the user clicks the button.
    public void ShowAd()
    {
        // Then show the ad:
        Advertisement.Show(_adUnitId, this);
    }

    // Implement the Show Listener's OnUnityAdsShowComplete callback method to determine if the user gets a reward:
    public void OnUnityAdsShowComplete(string adUnitId, UnityAdsShowCompletionState showCompletionState)
    {
        if (adUnitId.Equals(_adUnitId) && showCompletionState.Equals(UnityAdsShowCompletionState.COMPLETED))
        {
            Debug.LogWarning("OnUnityAdsShowComplete");
            // Grant a reward.

            StartCoroutine(GiveNukeBallFromAds());
        }
    }

    private IEnumerator GiveNukeBallFromAds()
    {
        yield return new WaitForSecondsRealtime(0.05f);

        _levelManager.SetNukeBallFromAds();
    }

    // Implement Load and Show Listener error callbacks:
    public void OnUnityAdsFailedToLoad(string adUnitId, UnityAdsLoadError error, string message)
    {
        Debug.LogWarning("OnUnityAdsFailedToLoad");
        // Use the error details to determine whether to try to load another ad.
        StartCoroutine(ShowToast(_failedToLoadMessage, true));
    }

    public void OnUnityAdsShowFailure(string adUnitId, UnityAdsShowError error, string message)
    {
        Debug.LogWarning("OnUnityAdsShowFailure");
        // Use the error details to determine whether to try to load another ad.
        StartCoroutine(ShowToast(_failedToShowMessage, true));
    }

    private IEnumerator ShowToast(string message, bool isLongToast)
    {
        yield return new WaitForSecondsRealtime(0.05f);

        ToastManager.Instance.CreateToast(message, isLongToast);
    }

    public void OnUnityAdsShowStart(string adUnitId)
    {
        Debug.LogWarning("OnUnityAdsShowStart");
    }

    public void OnUnityAdsShowClick(string adUnitId)
    {
        Debug.LogWarning("OnUnityAdsShowClick");
    }
}