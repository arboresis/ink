using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class BannerAd : MonoBehaviour
{
    [SerializeField] BannerPosition _bannerPosition = BannerPosition.BOTTOM_CENTER;

    string _adUnitId = "Banner_Android";

    [SerializeField]
    private float _bannerLoadTimeout = 1f;

    private void Awake()
    {
        // Set the banner position:
        Advertisement.Banner.SetPosition(_bannerPosition);
    }

    private void OnEnable()
    {
        LoadBanner();
    }

    // Implement a method to call when the Load Banner button is clicked:
    public void LoadBanner()
    {
        if (ProVersionPurchaser.HasProVersion)
        {
            return;
        }

        // Set up options to notify the SDK of load events:
        BannerLoadOptions options = new BannerLoadOptions
        {
            loadCallback = OnBannerLoaded,
            errorCallback = OnBannerError
        };

        // Load the Ad Unit with banner content:
        Advertisement.Banner.Load(_adUnitId, options);
    }

    // Implement code to execute when the loadCallback event triggers:
    void OnBannerLoaded()
    {
        Debug.Log("Banner loaded");
        ShowBannerAd();
    }

    // Implement code to execute when the load errorCallback event triggers:
    void OnBannerError(string message)
    {
        if (ProVersionPurchaser.HasProVersion)
        {
            return;
        }

        Debug.Log($"Banner Error: {message}");
        Invoke("LoadBanner", _bannerLoadTimeout);
    }

    // Implement a method to call when the Show Banner button is clicked:
    void ShowBannerAd()
    {
        if (ProVersionPurchaser.HasProVersion)
        {
            return;
        }

        // Set up options to notify the SDK of show events:
        BannerOptions options = new BannerOptions
        {
            clickCallback = OnBannerClicked,
            hideCallback = OnBannerHidden,
            showCallback = OnBannerShown
        };

        // Show the loaded Banner Ad Unit:
        Advertisement.Banner.Show(_adUnitId, options);
    }

    // Implement a method to call when the Hide Banner button is clicked:
    void HideBannerAd()
    {
        // Hide the banner:
        Advertisement.Banner.Hide();
    }

    void OnBannerClicked() { }
    void OnBannerShown() { }
    void OnBannerHidden() { }
}