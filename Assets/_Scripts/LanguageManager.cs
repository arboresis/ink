using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Language
{
    English = 0,
    French = 1,
    German = 2,
    Italian = 3,
    Polish = 4,
    Portuguese = 5,
    Russian = 6,
    SimplifiedChinese = 7,
    Spanish = 8
}

public class LanguageManager : MonoBehaviour
{
    public static LanguageManager Instance
    {
        get
        {
            return _Instance;
        }
    }

    private static LanguageManager _Instance;

    public const string LANGUAGE_PLAYER_PREFS_KEY = "GameLanguage";

    [SerializeField]
    private LevelManager _levelManager;

    [SerializeField]
    private LanguageDependentElement[] _languageDependentElements;

    public Language GameLanguage;

    [Space]

    [SerializeField]
    private Toggle[] _mainMenuLanguageToggles;

    [SerializeField]
    private Toggle[] _tutorialMenuLanguageToggles;

    [Space]

    public string[] PaintedPercentageSuffixes;

    public string[] ClearedPercentageSuffixes;

    [Space]

    [SerializeField]
    private Image[] _flagImages;

    [SerializeField]
    private Sprite[] _regularFlagSprites;

    [SerializeField]
    private Sprite[] _invertedFlagSprites;

    // Start is called before the first frame update
    void Awake()
    {
        _Instance = this;

        _languageDependentElements = FindObjectsOfType<LanguageDependentElement>(true);

        ReobtainLanguage();
    }

    public void ReobtainLanguage()
    {
        UpdateLanguage(GetPlayerPrefsLanguageIndex());
    }

    public static int GetPlayerPrefsLanguageIndex()
    {
        return PlayerPrefs.GetInt(LANGUAGE_PLAYER_PREFS_KEY, 0);
    }

    public void UpdateLanguage(int lang)
    {
        GameLanguage = (Language)lang;
        PlayerPrefs.SetInt(LANGUAGE_PLAYER_PREFS_KEY, lang);

        _mainMenuLanguageToggles[lang].isOn = true;
        _tutorialMenuLanguageToggles[lang].isOn = true;
        _levelManager.UpdatePercentageSuffix();

        UpdateAllLanguageDependentElements();
    }

    private void UpdateAllLanguageDependentElements()
    {
        foreach (LanguageDependentElement languageElement in _languageDependentElements)
        {
            languageElement.UpdateElement(GameLanguage);
        }
    }

    public void UpdateFlagImages(bool inverted)
    {
        for (int i = 0; i < _flagImages.Length; i++)
        {
            if (inverted)
            {
                _flagImages[i].sprite = _invertedFlagSprites[i];
            }
            else
            {
                _flagImages[i].sprite = _regularFlagSprites[i];
            }
        }
    }
}
