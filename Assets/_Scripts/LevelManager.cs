﻿using PaintIn3D;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityStandardAssets.ImageEffects;

public enum BallType
{
    Small,
    Medium,
    Big,
    Nuke
}

public class LevelManager : MonoBehaviour
{
    [Header("Debug Parameters")]

    [SerializeField]
    private bool _shouldResetTutorialFlag = false;

    [SerializeField]
    private bool _debugPercentageNeeded = true;

    [SerializeField]
    private bool _useDebugLevelSkip;

    [SerializeField]
    private string _skipToLevel;

    [Header("____________________________")]
    [Space]
    
    [SerializeField]
    private GameObject _tutorial;

    [SerializeField]
    private Level _tutorialLevel;

    public const string TUTORIAL_COMPLETED_FLAG = "TutorialCompleted";

    [Header("____________________________")]
    [Space]

    #region Color curve management

    [SerializeField]
    private ColorCorrectionCurves _colorCorrectionCurves;

    [Space]
    [SerializeField]
    private AnimationCurve _transitionCurve;

    [SerializeField]
    private AnimationCurve _regularColorCurve;

    [SerializeField]
    private AnimationCurve _invertedColorCurve;

    [SerializeField]
    private P3dPaintDecal _inkBallPaintDecal;

    [SerializeField]
    private P3dTapThrow _inkBallManager;

    private bool Inverted
    {
        get
        {
            return _inverted;
        }

        set
        {
            _inverted = value;

            if (value == true)
            {
                _smallInkBall.BlendMode = P3dBlendMode.SubtractiveSoft(new Vector4(1, 1, 1, 1));
                _mediumInkBall.BlendMode = P3dBlendMode.SubtractiveSoft(new Vector4(1, 1, 1, 1));
                _bigInkBall.BlendMode = P3dBlendMode.SubtractiveSoft(new Vector4(1, 1, 1, 1));
                _nukeInkBall.BlendMode = P3dBlendMode.SubtractiveSoft(new Vector4(1, 1, 1, 1));

                _colorCounter.Inverse = true;
                UpdatePercentageSuffix();
            }
            else
            {
                _smallInkBall.BlendMode = P3dBlendMode.Premultiplied(new Vector4(1, 1, 1, 1));
                _mediumInkBall.BlendMode = P3dBlendMode.Premultiplied(new Vector4(1, 1, 1, 1));
                _bigInkBall.BlendMode = P3dBlendMode.Premultiplied(new Vector4(1, 1, 1, 1));
                _nukeInkBall.BlendMode = P3dBlendMode.Premultiplied(new Vector4(1, 1, 1, 1));

                _colorCounter.Inverse = false;
                UpdatePercentageSuffix();
            }

            LanguageManager.Instance.UpdateFlagImages(_inverted);
        }
    }

    public void UpdatePercentageSuffix()
    {
        _colorCounter.UpdatePercentageSuffixLanguage(_inverted);
    }

    private bool _inverted = false;

    private Coroutine _inversionCoroutine;

    //Starts the color inversion coroutine
    public void InvertColors()
    {
        if (_inversionCoroutine != null)
        {
            StopCoroutine(_inversionCoroutine);
        }

        _inversionCoroutine = StartCoroutine(InvertColorCurves());
    }

    //Instantly inverts the colors
    public void InstantColorInversion()
    {
        if (_inversionCoroutine != null)
        {
            StopCoroutine(_inversionCoroutine);
        }

        Inverted = true;
        _colorCorrectionCurves.SetColorCurves(_invertedColorCurve);
    }

    //Coroutine to invert the colors
    private IEnumerator InvertColorCurves()
    {
        AnimationCurve curve;

        bool transitionComplete = false;

        float transitionTime = 0f;

        float tempRepositionDur;

        tempRepositionDur = 1f;

        if (Inverted)
        {
            Inverted = false;
        }
        else
        {
            Inverted = true;
        }

        while (!transitionComplete)
        {
            //Increase the current animation time
            transitionTime += Time.deltaTime;

            //Clamp the animation time between the beginning and the full duration
            transitionTime = Mathf.Clamp(transitionTime, 0f, tempRepositionDur);

            //Calculate the animation's progress
            float transitionProgress = transitionTime / tempRepositionDur;

            //If the progress is above 99%
            if (transitionProgress > 0.99f)
            {
                //Set the animation as complete
                transitionComplete = true;

                if (Inverted)
                {
                    _colorCorrectionCurves.SetColorCurves(_invertedColorCurve);
                }
                else
                {
                    _colorCorrectionCurves.SetColorCurves(_regularColorCurve);
                }

                _inversionCoroutine = null;

                yield break;
            }

            AnimationCurve
                curve1 = new AnimationCurve(),
                curve2 = new AnimationCurve();

            if (Inverted)
            {
                curve1 = _regularColorCurve;
                curve2 = _invertedColorCurve;
            }
            else
            {
                curve1 = _invertedColorCurve;
                curve2 = _regularColorCurve;
            }

            float
                minY = Mathf.Lerp(curve1.keys[0].value, curve2.keys[0].value, _transitionCurve.Evaluate(transitionProgress)),
                maxY = Mathf.Lerp(curve1.keys[1].value, curve2.keys[1].value, _transitionCurve.Evaluate(transitionProgress));

            curve = new AnimationCurve(new Keyframe(0f, minY), new Keyframe(1f, maxY));

            _colorCorrectionCurves.SetColorCurves(curve);

            yield return new WaitForEndOfFrame();
        }
    }

    #endregion Color curve management

    #region Level management

    [Space]
    private List<int> _invertedLevels;

    private List<Level> _levels;

    private int CurrentLevel
    {
        get
        {
            return _currentLevel;
        }

        set
        {
            _currentLevel = value;

            if(_currentLevel < 0)
            {
                return;
            }

            SetLevelForceType();
        }
    }

    private int _currentLevel;

    private float _targetPercentage = 1f;

    [SerializeField]
    private Animator _levelTransitionAnimator;

    [SerializeField]
    private float _interludeDurationPercentage = 1f / 6f;

    [SerializeField]
    private MultiTargetGraphicButton _nextLevelButton;
    
    [SerializeField]
    private MultiTargetGraphicButton _tutorialNextLevelButton;

    [Space]
    [SerializeField]
    private UnityEvent OnNextLevelButtonInteractableEvent;

    [Space]
    [SerializeField]
    private UnityEvent OnNextLevelButtonNonInteractableEvent;

    [Space]
    [SerializeField]
    private TextMeshProUGUI
        _currentLevelIndicator,
        _nextLevelIndicator;

    [SerializeField]
    private P3dChannelCounterFill _colorCounter;

    private int totalAmountOfSmallInkBalls
    {
        get
        {
            return _totalAmountOfSmallInkBalls;
        }

        set
        {
            _totalAmountOfSmallInkBalls = value;
            amountOfSmallInkBalls = value;
        }
    }

    private int totalAmountOfMediumInkBalls
    {
        get
        {
            return _totalAmountOfMediumInkBalls;
        }

        set
        {
            _totalAmountOfMediumInkBalls = value;
            amountOfMediumInkBalls = value;
        }
    }

    private int totalAmountOfBigInkBalls
    {
        get
        {
            return _totalAmountOfBigInkBalls;
        }

        set
        {
            _totalAmountOfBigInkBalls = value;
            amountOfBigInkBalls = value;
        }
    }

    private int totalAmountOfNukeInkBalls
    {
        get
        {
            return _totalAmountOfNukeInkBalls;
        }

        set
        {
            _totalAmountOfNukeInkBalls = value;
            amountOfNukeInkBalls = value;
        }
    }

    private int _totalAmountOfSmallInkBalls;
    private int _totalAmountOfMediumInkBalls;
    private int _totalAmountOfBigInkBalls;
    private int _totalAmountOfNukeInkBalls;

    private int amountOfSmallInkBalls
    {
        get
        {
            return _amountOfSmallInkBalls;
        }

        set
        {
            _amountOfSmallInkBalls = value;

            if (value >= 0)
            {
                _smallInkBallsCounter.text = value.ToString();
            }
            else
            {
                _smallInkBallsCounter.text = "∞";
            }
        }
    }

    private int amountOfMediumInkBalls
    {
        get
        {
            return _amountOfMediumInkBalls;
        }

        set
        {
            _amountOfMediumInkBalls = value;

            if (value >= 0)
            {
                _mediumInkBallsCounter.text = value.ToString();
            }
            else
            {
                _mediumInkBallsCounter.text = "∞";
            }
        }
    }

    private int amountOfBigInkBalls
    {
        get
        {
            return _amountOfBigInkBalls;
        }

        set
        {
            _amountOfBigInkBalls = value;

            if (value >= 0)
            {
                _bigInkBallsCounter.text = value.ToString();
            }
            else
            {
                _bigInkBallsCounter.text = "∞";
            }
        }
    }

    private int amountOfNukeInkBalls
    {
        get
        {
            return _amountOfNukeInkBalls;
        }

        set
        {
            _amountOfNukeInkBalls = value;

            if (value >= 0)
            {
                _nukeInkBallsCounter.text = value.ToString();
            }
            else
            {
                _nukeInkBallsCounter.text = "∞";
            }
        }
    }

    private int
        _amountOfSmallInkBalls,
        _amountOfMediumInkBalls,
        _amountOfBigInkBalls,
        _amountOfNukeInkBalls;

    [Space]
    [SerializeField] private TextMeshProUGUI _currentInkBallsCounter;
    [SerializeField] private TextMeshProUGUI _smallInkBallsCounter;
    [SerializeField] private TextMeshProUGUI _mediumInkBallsCounter;
    [SerializeField] private TextMeshProUGUI _bigInkBallsCounter;
    [SerializeField] private TextMeshProUGUI _nukeInkBallsCounter;

    [Space]
    [SerializeField] private P3dPaintDecal _smallInkBall;
    [SerializeField] private P3dPaintDecal _mediumInkBall;
    [SerializeField] private P3dPaintDecal _bigInkBall;
    [SerializeField] private P3dPaintDecal _nukeInkBall;

    [Space]
    [SerializeField] private GameObject _smallPredictors;
    [SerializeField] private GameObject _mediumPredictors;
    [SerializeField] private GameObject _bigPredictors;
    [SerializeField] private GameObject _nukePredictors;

    [Space]
    [SerializeField] private AudioClip _smallBallReleaseSound;
    [SerializeField] private AudioClip _smallBallHitSound;

    [SerializeField] private AudioClip _mediumBallReleaseSound;
    [SerializeField] private AudioClip _mediumBallHitSound;

    [SerializeField] private AudioClip _bigBallReleaseSound;
    [SerializeField] private AudioClip _bigBallHitSound;

    [SerializeField] private AudioClip _nukeBallReleaseSound;
    [SerializeField] private AudioClip _nukeBallHitSound;

    [Space]
    [SerializeField] private BallSizeSelectionMenu _ballSizeSelectionMenu;

    [SerializeField]
    private Animator _initialLogoScreens;

    public BallType CurrentBallType
    {
        get
        {
            return _currentBallType;
        }

        set
        {
            _currentBallType = value;

            switch (value)
            {
                case BallType.Small:
                    _inkBallManager.Prefab = _smallInkBall.gameObject;
                    _inkBallManager.TrajectoryPredictorsParent = _smallPredictors;
                    _inkBallManager.BallReleaseSound = _smallBallReleaseSound;
                    _inkBallManager.BallHitSound = _smallBallHitSound;
                    break;
                case BallType.Medium:
                    _inkBallManager.Prefab = _mediumInkBall.gameObject;
                    _inkBallManager.TrajectoryPredictorsParent = _mediumPredictors;
                    _inkBallManager.BallReleaseSound = _mediumBallReleaseSound;
                    _inkBallManager.BallHitSound = _mediumBallHitSound;
                    break;
                case BallType.Big:
                    _inkBallManager.Prefab = _bigInkBall.gameObject;
                    _inkBallManager.TrajectoryPredictorsParent = _bigPredictors;
                    _inkBallManager.BallReleaseSound = _bigBallReleaseSound;
                    _inkBallManager.BallHitSound = _bigBallHitSound;
                    break;
                case BallType.Nuke:
                    _inkBallManager.Prefab = _nukeInkBall.gameObject;
                    _inkBallManager.TrajectoryPredictorsParent = _nukePredictors;
                    _inkBallManager.BallReleaseSound = _nukeBallReleaseSound;
                    _inkBallManager.BallHitSound = _nukeBallHitSound;
                    break;
            }
        }
    }

    public UnityEvent LevelSelectionHideEvent;

    private BallType _currentBallType;

    [SerializeField]
    private UnityEvent _onGameFinishedEvent;

    private bool _firstTimeLoadingLevel = true;

    private float _numberOfLevelsPassed = -1;

    [SerializeField]
    private int _amountOfLevelsUntilAd = 3;

    [SerializeField]
    private InterstitialAd _interstitialAd;

    [SerializeField]
    private RewardedAdsButton _nukeRewardAd;

    private bool _interstitialAdHasBeenShown = false;

    public List<GameObject> ActiveBalls = new List<GameObject>();

    // Start is called before the first frame update
    void Awake()
    {
        _levels = new List<Level>(transform.childCount);

        for (int i = 0; i < transform.childCount; i++)
        {
            _levels.Add(transform.GetChild(i).GetComponent<Level>());

            transform.GetChild(i).GetComponent<Level>().GetLevelName();

            //Deactivate the level
            transform.GetChild(i).gameObject.SetActive(false);
        }

        _invertedLevels = new List<int>();

        for (int i = 0; i < _levels.Count; i++)
        {
            if (_levels[i].Inverted)
            {
                _invertedLevels.Add(i);
            }
        }

        //If the debug parameters should be used and the player is in the Editor or in a Debug Build, load the specified level instead
        if (_useDebugLevelSkip && (Application.isEditor || Debug.isDebugBuild))
        {
            int targetLevelIndex = 0;

            for (int i = 0; i < _levels.Count; i++)
            {
                if (_levels[i].name.Contains(_skipToLevel))
                {
                    targetLevelIndex = i;
                }
            }

            CurrentLevel = targetLevelIndex;
        }
        else
        {
            CurrentLevel = PlayerPrefs.GetInt("LastPlayedLevel", 0);
            if (CurrentLevel >= _levels.Count)
            {
                CurrentLevel = 0;
            }
        }

        //If the last played level was an inverted level, instantly invert the colors
        if (IsInvertedLevel())
        {
            InstantColorInversion();
        }
        else
        {
            Inverted = false;
        }

        StartCoroutine(ShowInitialLogoScreens());
    }

    private IEnumerator ShowInitialLogoScreens()
    {
        float initialLogoScreensDuration = _initialLogoScreens.GetCurrentAnimatorStateInfo(0).length;

        _initialLogoScreens.Play("InitialLogoScreens");

        yield return new WaitForSeconds(initialLogoScreensDuration - 1f);

        if (_shouldResetTutorialFlag && (Application.isEditor || Debug.isDebugBuild))
        {
            PlayerPrefs.SetInt(TUTORIAL_COMPLETED_FLAG, 0);
        }

        if (PlayerPrefs.GetInt(TUTORIAL_COMPLETED_FLAG, 0) > 0)
        {
            SelectLevel(CurrentLevel);
        }
        else
        {
            StartTutorial();
        }

        yield return new WaitForSeconds(1f);

        _initialLogoScreens.gameObject.SetActive(false);

        yield break;
    }

    private void StartTutorial()
    {
        _tutorial.SetActive(true);
    }

    public void ClearBallsAndFingers()
    {
        DestroyAllActiveBalls();
        _inkBallManager.ClearCachedFingers();
    }

    public void ResetTutorialLevel()
    {
        ClearBallsAndFingers();
        SetLevelForceType();
        _tutorialLevel.ResetCurrentLevel();
    }

    public void FinishTutorial()
    {
        PlayerPrefs.SetInt(TUTORIAL_COMPLETED_FLAG, 1);
        SelectLevel(0);
    }

    public void FinishTutorialAndResume()
    {
        PlayerPrefs.SetInt(TUTORIAL_COMPLETED_FLAG, 1);
        ResumeLastPlayedLevel();
    }

    private void SaveLevelIndexes()
    {
        //Get the furthest level that the player has seen
        int furthestLevel = PlayerPrefs.GetInt("FurthestLevel", 0);

        //Save the current level
        PlayerPrefs.SetInt("LastPlayedLevel", CurrentLevel);
        Debug.LogWarning("Setting last played level as: " + CurrentLevel);

        //If the current level is above the furthest saved level, save the current level as the furthest
        if (CurrentLevel > furthestLevel)
        {
            PlayerPrefs.SetInt("FurthestLevel", CurrentLevel);
            Debug.LogWarning("Setting furthest played level as: " + CurrentLevel);
        }
    }

    public void SetCurrentLevel(int levelIndex)
    {
        if (CurrentLevel != levelIndex)
        {
            CurrentLevel = levelIndex;
            SaveLevelIndexes();
            SetLevelForceType();
        }
    }

    public bool ShouldUseDebugPercentageNeeded()
    {
        return _debugPercentageNeeded;
    }

    public void SetTargetPercentage(float targetPercentage)
    {
        _targetPercentage = targetPercentage / 100f;
    }

    public bool CanThrowInkBall()
    {
        switch (CurrentBallType)
        {
            case BallType.Small:
                return amountOfSmallInkBalls == -1 || amountOfSmallInkBalls > 0;
            case BallType.Medium:
                return amountOfMediumInkBalls == -1 || amountOfMediumInkBalls > 0;
            case BallType.Big:
                return amountOfBigInkBalls == -1 || amountOfBigInkBalls > 0;
            case BallType.Nuke:
                return _amountOfNukeInkBalls == -1 || _amountOfNukeInkBalls > 0;
            default:
                return true;
        }
    }

    public bool HasInkBallsOfType(BallType ballType)
    {
        switch (ballType)
        {
            case BallType.Small:
                return amountOfSmallInkBalls == -1 || amountOfSmallInkBalls > 0;
            case BallType.Medium:
                return amountOfMediumInkBalls == -1 || amountOfMediumInkBalls > 0;
            case BallType.Big:
                return amountOfBigInkBalls == -1 || amountOfBigInkBalls > 0;
            default:
                return true;
        }
    }

    public void DeductInkBall()
    {
        switch (CurrentBallType)
        {
            case BallType.Small:
                if (amountOfSmallInkBalls != -1)
                    amountOfSmallInkBalls -= 1;
                UpdateBallCounter(amountOfSmallInkBalls);
                break;
            case BallType.Medium:
                if (amountOfMediumInkBalls != -1)
                    amountOfMediumInkBalls -= 1;
                UpdateBallCounter(amountOfMediumInkBalls);
                break;
            case BallType.Big:
                if (amountOfBigInkBalls != -1)
                    amountOfBigInkBalls -= 1;
                UpdateBallCounter(amountOfBigInkBalls);
                break;
            case BallType.Nuke:
                if (_amountOfNukeInkBalls != -1)
                    _amountOfNukeInkBalls -= 1;
                UpdateBallCounter(_amountOfNukeInkBalls);
                break;
        }
    }

    private void UpdateBallCounter(int amount)
    {
        if (amount > -1)
        {
            _currentInkBallsCounter.text = amount.ToString();
        }
        else
        {
            _currentInkBallsCounter.text = "∞";
        }
    }

    public void ResetInkBallsAmounts(int amountOfSmall, int amountOfMedium, int amountOfBig, int amountOfNukes)
    {
        totalAmountOfSmallInkBalls = amountOfSmall;
        totalAmountOfMediumInkBalls = amountOfMedium;
        totalAmountOfBigInkBalls = amountOfBig;
        totalAmountOfNukeInkBalls = amountOfNukes;

        SetSmallBall();
    }

    public void SetSmallBall()
    {
        CurrentBallType = BallType.Small;
        UpdateBallCounter(amountOfSmallInkBalls);
        _ballSizeSelectionMenu.ShowBallType(BallType.Small);
        _ballSizeSelectionMenu.HideBallSizeMenu();
    }

    public void SetMediumBall()
    {
        CurrentBallType = BallType.Medium;
        UpdateBallCounter(amountOfMediumInkBalls);
        _ballSizeSelectionMenu.ShowBallType(BallType.Medium);
        _ballSizeSelectionMenu.HideBallSizeMenu();
    }

    public void SetBigBall()
    {
        CurrentBallType = BallType.Big;
        UpdateBallCounter(amountOfBigInkBalls);
        _ballSizeSelectionMenu.ShowBallType(BallType.Big);
        _ballSizeSelectionMenu.HideBallSizeMenu();
    }

    public void SetNukeBall()
    {
        CurrentBallType = BallType.Nuke;
        UpdateBallCounter(_amountOfNukeInkBalls);
        _ballSizeSelectionMenu.ShowBallType(BallType.Nuke);
        _ballSizeSelectionMenu.HideBallSizeMenu();
    }

    public void SetNukeBallFromAds()
    {
        CurrentBallType = BallType.Nuke;
        _amountOfNukeInkBalls = 1;
        UpdateBallCounter(_amountOfNukeInkBalls);
        _ballSizeSelectionMenu.ShowBallType(BallType.Nuke);
        _ballSizeSelectionMenu.HideBallSizeMenu();
    }

    public void ResetNextLevelButton()
    {
        if (_tutorial.activeInHierarchy == false)
        {
            _nextLevelButton.interactable = false;
        }
        else
        {
            _tutorialNextLevelButton.interactable = false;
        }

        OnNextLevelButtonNonInteractableEvent.Invoke();
    }

    public void CheckPercentage(float percentage)
    {
        if (percentage >= _targetPercentage && GameManager.ShouldAllowGameControl)
        {
            if (_tutorial.activeInHierarchy == false)
            {
                _nextLevelButton.interactable = true;
            }
            else
            {
                _tutorialNextLevelButton.interactable = true;
            }

            if (_tutorial.activeInHierarchy)
            {
                _tutorial.GetComponent<Tutorial>().OnTutorialLevelPercentageReached();
            }

            OnNextLevelButtonInteractableEvent.Invoke();
        }
        else
        {
            ResetNextLevelButton();
        }
    }

    public float GetPercentage()
    {
        return _colorCounter.CurrentPercentage;
    }

    public void ResetCurrentLevel()
    {
        DestroyAllActiveBalls();
        _inkBallManager.ClearCachedFingers();
        SetLevelForceType();
        _levels[CurrentLevel].ResetCurrentLevel();
    }

    private void SetLevelForceType()
    {
        switch (_levels[_currentLevel].LevelForceType)
        {
            case ForceType.Regular:
                _inkBallManager.GravityForce = Physics.gravity;
                break;
            case ForceType.None:
                _inkBallManager.GravityForce = Vector3.zero;
                break;
            case ForceType.Inverted:
                _inkBallManager.GravityForce = -Physics.gravity;
                break;
        }

        if (CheatsManager.HasMoonGravityEnabled())
        {
            _inkBallManager.GravityForce *= CheatsManager.MoonGravityMultiplier;
        }
    }

    public void DestroyAllActiveBalls()
    {
        while (ActiveBalls.Count > 0)
        {
            GameObject ball = ActiveBalls[0];

            if (ball != null)
            {
                ActiveBalls.Remove(ball);
                ball.GetComponent<P3dDestroyer>().DestroyNow();
            }
        }
    }

    public bool IsInvertedLevel()
    {
        return _invertedLevels.Contains(CurrentLevel);
    }

    public void OnCurrentLevelCompleted()
    {
        _levels[CurrentLevel].LevelCompleted();
        _inkBallManager.ClearCachedFingers();
        GameManager.ShouldAllowGameControl = false;
    }

    public bool ShouldRevealEndResult()
    {
        return (_invertedLevels.Contains(CurrentLevel) == false && _colorCounter.cachedImage.fillAmount != 1f && CheatsManager.HasInktinArboresitinoEnabled() == false);
    }

    public void FadeLevelOut(UnityEvent levelEvent, bool shouldStartEvent = false, bool shouldCurrentLevelBeEmpty = false, bool shouldOverlookLastLevel = false)
    {
        if (CurrentLevel < _levels.Count - 1 || shouldOverlookLastLevel)
        {
            StartCoroutine(FadeOut(shouldStartEvent, levelEvent, shouldCurrentLevelBeEmpty, shouldOverlookLastLevel));
        }
        else
        {
            _onGameFinishedEvent.Invoke();
        }
    }

    private IEnumerator FadeOut(bool shouldStartEvent, UnityEvent levelEvent, bool shouldCurrentLevelBeEmpty, bool shouldReturnToFirstLevel)
    {
        GameManager.ShouldAllowGameControl = false;

        int targetLevel;

        if(shouldReturnToFirstLevel == false)
        {
            targetLevel = CurrentLevel + 1;
        }
        else
        {
            targetLevel = 0;
        }

        if (shouldReturnToFirstLevel == false)
        {
            _levelTransitionAnimator.Play("LevelFadeOut");

            yield return new WaitUntil(() => _levelTransitionAnimator.GetCurrentAnimatorStateInfo(0).IsName("LevelFadeOut"));
            yield return new WaitForSeconds(_levelTransitionAnimator.GetCurrentAnimatorStateInfo(0).length);
        }

        ToggleLevel(CurrentLevel, false);

        //If the next level is inverted and the colors aren't inverted
        if (Inverted == false && _invertedLevels.Contains(targetLevel) == true)
        {
            InvertColors();
            yield return new WaitUntil(() => _inversionCoroutine == null);
        }
        //If the next level isn't inverted and the colors are inverted
        else if (Inverted == true && _invertedLevels.Contains(targetLevel) == false)
        {
            InvertColors();
            yield return new WaitUntil(() => _inversionCoroutine == null);
        }

        //Adjusted so that the levels aren't presented as starting from 0
        SetLevelIndicators(targetLevel, shouldCurrentLevelBeEmpty);

        if (shouldStartEvent == false)
        {
            FadeLevelIn(targetLevel, shouldCurrentLevelBeEmpty);
        }
        else
        {
            levelEvent.Invoke();
        }

        if (_tutorial.activeInHierarchy)
        {
            _tutorial.SetActive(false);
        }
    }

    public void FadeLevelIn(int targetLevel, bool shouldSkipInterlude = false)
    {
        StartCoroutine(FadeIn(targetLevel, shouldSkipInterlude));
    }

    public void ShowAdIfEnoughLevelsPassed(float numberToAdd = 1f)
    {
        if (ShouldHideAds() == false)
        {
            _numberOfLevelsPassed += numberToAdd;

            //If the number of levels is a multiple of the defined amount of levels to show an ad
            if (Mathf.RoundToInt(_numberOfLevelsPassed) >= _amountOfLevelsUntilAd)
            {
                _numberOfLevelsPassed = 0;
                _interstitialAd.LoadAd();
            }
        }
    }

    private IEnumerator FadeIn(int targetLevel, bool shouldSkipInterlude)
    {
        if (_tutorial.activeInHierarchy)
        {
            _tutorial.SetActive(false);
        }

        GameManager.ShouldAllowGameControl = false;

        ShowAdIfEnoughLevelsPassed();

        ToggleLevel(targetLevel, true);

        if (shouldSkipInterlude == false)
        {
            _levelTransitionAnimator.Play("LevelFadeIn");
            yield return new WaitUntil(() => _levelTransitionAnimator.GetCurrentAnimatorStateInfo(0).IsName("LevelFadeIn"));
            yield return new WaitForSeconds(_levelTransitionAnimator.GetCurrentAnimatorStateInfo(0).length);
        }
        else
        {
            _levelTransitionAnimator.Play("LevelFadeIn", 0, _interludeDurationPercentage);
            yield return new WaitUntil(() => _levelTransitionAnimator.GetCurrentAnimatorStateInfo(0).IsName("LevelFadeIn"));
            yield return new WaitForSeconds(_levelTransitionAnimator.GetCurrentAnimatorStateInfo(0).length * (1f - _interludeDurationPercentage));
        }

        GameManager.ShouldAllowGameControl = true;
        _levels[_currentLevel].ResetTimer();
    }

    private void ToggleLevel(int levelIndex, bool newState)
    {
        if (levelIndex >= 0)
        {
            _levels[levelIndex].gameObject.SetActive(newState);
        }
    }

    public void SetLevelIndicators(int nextLevel, bool shouldCurrentLevelBeEmpty = false)
    {
        if (shouldCurrentLevelBeEmpty == false)
        {
            _currentLevelIndicator.text = nextLevel.ToString();
        }
        else
        {
            _currentLevelIndicator.text = string.Empty;
        }

        _nextLevelIndicator.text = (nextLevel + 1).ToString();
    }

    public void SelectLevel(int levelIndex)
    {
        DeactivateAllLevels();
        CurrentLevel = levelIndex - 1;

        //Check whether or not this is the first time loading a level; if it is, skip the interlude and jump straight into the transition animation
        if (_firstTimeLoadingLevel == false)
        {
            FadeLevelOut(new UnityEvent(), false, true);
        }
        else
        {
            SetLevelIndicators(CurrentLevel + 1, true);
            FadeLevelIn(levelIndex, true);
            _firstTimeLoadingLevel = false;
        }
    }

    public void HideLevelSelectionMenu()
    {
        LevelSelectionHideEvent.Invoke();
    }

    public void DeactivateAllLevels()
    {
        for (int i = 0; i < _levels.Count; i++)
        {
            _levels[i].gameObject.SetActive(false);
        }
    }

    public void ResumeLastPlayedLevel()
    {
        SelectLevel(PlayerPrefs.GetInt("LastPlayedLevel", 0));
    }

    public List<Level> GetLevelList()
    {
        return _levels;
    }

    #endregion Level management

    [SerializeField]
    private Animator _gameUIAnimator;

    [SerializeField]
    private Animator _finalLevelAnimator;

    public void PlayEndingSequence()
    {
        StartCoroutine(EndingSequence());
    }

    private IEnumerator EndingSequence()
    {
        _gameUIAnimator.Play("AllButtonsFade");

        _finalLevelAnimator.Play("FinalLevelCompleted");
        yield return new WaitUntil(() => _finalLevelAnimator.GetCurrentAnimatorStateInfo(0).IsName("FinalLevelCompleted"));
        yield return new WaitForSeconds(_finalLevelAnimator.GetCurrentAnimatorStateInfo(0).length);

        FadeLevelOut(new UnityEvent(), shouldCurrentLevelBeEmpty: true, shouldOverlookLastLevel: true);

        _gameUIAnimator.Play("MainMenuDefault");
    }

    private bool ShouldHideAds()
    {
        bool
            isDebugApp = Application.isEditor, //|| Debug.isDebugBuild,
            isProVersion = ProVersionPurchaser.HasProVersion;

        //Hide ads if it's a debug version or the pro version
        return isDebugApp || isProVersion;
    }
}
